#Calculator

>This is a hobby project.
It calculates retail/wholesale prices, material consumptions based on what the user is selecting.
>User can manually set VAT percent, wholesale discount percent, product price and consumption/kg.

###Technical details:
* JUnit tests ensure the correct working. 

* Data stored in database.

* DBMS is mysql.

* For view I used swingx.


