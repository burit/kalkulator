package hu.burit.calculator.view.forms;

import hu.burit.calculator.model.dao.ProductDAO;
import hu.burit.calculator.model.databaseconnection.DatabaseConnection;
import hu.burit.calculator.model.product.Product;
import hu.burit.calculator.view.indicator.Flag;

import java.awt.Color;
import java.awt.EventQueue;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;


import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

/**
 * @author <b>Tamas Buri, 
 * tamasburi.jr@gmail.com, 
 * 0036/70-555-777-5</b>
 */
public class ProductsForm extends JInternalFrame {
	private static final long serialVersionUID = 1L;
	private JTextField textFieldNetRetailPrice;
	private JTextField textFieldNetPurchasePriceEuro;
	@SuppressWarnings("rawtypes")
	private JComboBox comboBox;
	private JTextField textFieldProductConsumptionPerSquareMeter;
	private JButton buttonCancel;
	private JButton buttonSaveProducts;
	private JButton buttonModify;
	private ProductDAO productDAO;
	private JLabel labelProductConsumptionPerSquareMeter;
	private Product[] listOfComboboxSelection;
	private JLabel labelSuccesfulUpdate;
	private Product product;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ProductsForm frame = new ProductsForm();
					frame.setVisible(true);
					frame.setBackground(Color.lightGray);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * delete products price from the text fields
	 */
	public void clearTextFields(){
		textFieldNetRetailPrice.setText(String.valueOf(0));
		textFieldNetPurchasePriceEuro.setText(String.valueOf(0));
		textFieldProductConsumptionPerSquareMeter.setText(String.valueOf(0));
	}
	
	/**
	 * set products price in the text fields
	 * @param product use that Product object which price you want to set in the text field
	 */
	public void setTextFields(Product product){
		textFieldNetRetailPrice.setText(String.valueOf(product.getProductNetRetailSalePricePerKg()));
		textFieldNetPurchasePriceEuro.setText(String.valueOf(product.getProductNetCostPriceEuro()));
		textFieldProductConsumptionPerSquareMeter.setText(String.valueOf(product.getProductConsumptionKgPerSquareMeter()));
	}
	
	/**
	 * Create the frame.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ProductsForm() {
		StartForm.buttonShow.setVisible(false);
		setBounds(100, 100, 920, 189);
		getContentPane().setLayout(null);
		
		JLabel labelProductName = new JLabel("Product Name");
		labelProductName.setBounds(20, 22, 100, 14);
		getContentPane().add(labelProductName);
		
		JLabel labelNetRetailPrice = new JLabel("Net Retail Price");
		labelNetRetailPrice.setBounds(220, 22, 118, 14);
		getContentPane().add(labelNetRetailPrice);
		
		JLabel labelNetPurchasePriceEuro = new JLabel("Net purchase price euro");
		labelNetPurchasePriceEuro.setBounds(331, 22, 150, 14);
		getContentPane().add(labelNetPurchasePriceEuro);
		
		textFieldNetRetailPrice = new JTextField();
		textFieldNetRetailPrice.setBounds(220, 59, 86, 20);
		getContentPane().add(textFieldNetRetailPrice);
		textFieldNetRetailPrice.setColumns(10);
		
		textFieldNetPurchasePriceEuro = new JTextField();
		textFieldNetPurchasePriceEuro.setBounds(331, 59, 115, 20);
		getContentPane().add(textFieldNetPurchasePriceEuro);
		textFieldNetPurchasePriceEuro.setColumns(10);
	
		comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Plaese choose", "Eaa/a", "Eaa/b", "S gravel horizontal", "SC gravel horizontal", "SD gravel horizontal", "SM gravel horizontal", "SF gravel horizontal", "Sad22 horizontal", "Sil04", "Va01", "S gravel vertical", "SC gravel vertical", "SD gravel vertical", "SM gravel vertical", "SF gravel vertical", "Sad22 vertical" }));
		comboBox.setBounds(20, 59, 159, 20);
		getContentPane().add(comboBox);
		
		labelProductConsumptionPerSquareMeter = new JLabel("Product Consumption Per Square Meter");
		labelProductConsumptionPerSquareMeter.setBounds(503, 22, 239, 14);
		getContentPane().add(labelProductConsumptionPerSquareMeter);
		
		textFieldProductConsumptionPerSquareMeter = new JTextField();
		textFieldProductConsumptionPerSquareMeter.setBounds(503, 59, 205, 20);
		getContentPane().add(textFieldProductConsumptionPerSquareMeter);
		textFieldProductConsumptionPerSquareMeter.setColumns(10);
		
		buttonCancel = new JButton("Cancel");
		buttonCancel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Flag.setProductsForm(false);
				dispose();
			}
		});
		buttonCancel.setBounds(220, 111, 89, 23);
		getContentPane().add(buttonCancel);
		
		labelSuccesfulUpdate = new JLabel("You have successfully updated this product!");
		labelSuccesfulUpdate.setBounds(331, 115, 266, 14);
		getContentPane().add(labelSuccesfulUpdate);
		
		labelSuccesfulUpdate.setVisible(false);
		buttonModify = new JButton("Modify");
		buttonModify.setBounds(789, 58, 89, 23);
		getContentPane().add(buttonModify);
		buttonModify.setVisible(false);
		
		//we implement the ProductDAO to get all the elements from database
		productDAO = new ProductDAO(DatabaseConnection.getInstance().getConn());
		List<Product> list = productDAO.findAll();
		
		if(list == null || list.isEmpty() || list.size()<=16){
			buttonSaveProducts = new JButton("Save");
			buttonSaveProducts.setBounds(789, 58, 89, 23);
			getContentPane().add(buttonSaveProducts);
			comboBox.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					clearTextFields();
					switch (comboBox.getSelectedItem().toString()) {
					case "Eaa/a":
						insertUpdateProducts();
						break; 
					case "Eaa/b":
						insertUpdateProducts();
						break;
					case "S gravel horizontal":
						insertUpdateProducts();
						break;
					case "SC gravel horizontal":
						insertUpdateProducts();
						break;
					case "SD gravel horizontal":
						insertUpdateProducts();
						break;
					case "SM gravel horizontal":
						insertUpdateProducts();
						break;
					case "SF gravel horizontal":
						insertUpdateProducts();
						break;
					case "Sad22 horizontal":
						insertUpdateProducts();
						break;
					case "Sil04":
						insertUpdateProducts();
						break;
					case "Va01":
						insertUpdateProducts();
						break;
					case "S gravel vertical":
						insertUpdateProducts();
						break;
					case "SC gravel vertical":
						insertUpdateProducts();
						break;
					case "SD gravel vertical":
						insertUpdateProducts();
						break;
					case "SM gravel vertical":
						insertUpdateProducts();
						break;
					case "SF gravel vertical":
						insertUpdateProducts();
						break;
					case "Sad22 vertical":
						insertUpdateProducts();
						break;
					default:
						break;
					}
				}
			});			
		}else{		
			//TODO we already have all products, we don't need more
	}	
}
	
	/**
	 * we insert or update the products
	 */
	public void insertUpdateProducts(){
		labelSuccesfulUpdate.setVisible(false);
		listOfComboboxSelection = null;
		listOfComboboxSelection = productDAO.findArray(comboBox.getSelectedItem().toString());
		
		clearTextFields();
		
		if (listOfComboboxSelection[0] == null) {
			buttonSaveProducts.setVisible(true);
			buttonModify.setVisible(false);
			buttonSaveProducts.addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent arg0) {
					product = new Product(0,comboBox.getSelectedItem().toString(), Double.valueOf(textFieldNetRetailPrice.getText()), Double.valueOf(listOfComboboxSelection[0].getProductNetWholesaleSalePricePerKg()),Double.valueOf(textFieldNetPurchasePriceEuro.getText()),Double.valueOf(textFieldProductConsumptionPerSquareMeter.getText()));				
					productDAO.insert(product);
					labelSuccesfulUpdate.setVisible(true);
				}
			});
		} else{
			buttonSaveProducts.setVisible(false);
			buttonModify.setVisible(true);
			product = new Product(0,comboBox.getSelectedItem().toString(), Double.valueOf(listOfComboboxSelection[0].getProductNetRetailSalePricePerKg()), Double.valueOf(listOfComboboxSelection[0].getProductNetWholesaleSalePricePerKg()),Double.valueOf(listOfComboboxSelection[0].getProductNetCostPriceEuro()), Double.valueOf(listOfComboboxSelection[0].getProductConsumptionKgPerSquareMeter()));
			setTextFields(product);
			buttonModify.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					product.setProductNetRetailSalePricePerKg(Double.valueOf(textFieldNetRetailPrice.getText()));
					product.setProductNetCostPriceEuro(Double.valueOf(textFieldNetPurchasePriceEuro.getText()));
					product.setProductConsumptionKgPerSquareMeter(Double.valueOf(textFieldProductConsumptionPerSquareMeter.getText()));
				
					product.setId(Integer.valueOf(listOfComboboxSelection[0].getId()));
					productDAO.update(product);
					labelSuccesfulUpdate.setVisible(true);
					
				}
			});
		}
	}
}
