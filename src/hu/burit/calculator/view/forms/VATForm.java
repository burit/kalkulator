package hu.burit.calculator.view.forms;

import hu.burit.calculator.model.dao.VATDAO;
import hu.burit.calculator.model.databaseconnection.DatabaseConnection;
import hu.burit.calculator.model.utils.VAT;
import hu.burit.calculator.view.indicator.Flag;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.List;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * @author <b>Tamas Buri, 
 * tamasburi.jr@gmail.com, 
 * 0036/70-555-777-5</b>
 */
public class VATForm extends JInternalFrame {
	
	private static final long serialVersionUID = 1L;
	private JTextField textField;
	private VAT vatNumber;
	private VATDAO vATDAO;
	private JButton buttonSaveVat;
	private JButton buttonModifyVat;
	private JButton buttonCloseVat;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VATForm frame = new VATForm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the VAT frame.
	 */
	public VATForm() {
		setBounds(100, 100, 393, 135);
		getContentPane().setLayout(null);
	
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(130, 40, 86, 20);
		getContentPane().add(textField);
		
		JLabel label = new JLabel("VAT Percent");
		label.setBounds(34, 43, 86, 14);
		getContentPane().add(label);
		
		buttonCloseVat = new JButton("Close");
		buttonCloseVat.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				Flag.setVatForm(false);
				dispose();
			}
		});
		
		buttonCloseVat.setBounds(130, 72, 89, 23);
		getContentPane().add(buttonCloseVat);
		
		//implements VatDAO
		vATDAO = new VATDAO(DatabaseConnection.getInstance().getConn());
		final List<VAT> list = vATDAO.findAll();
	
		if(list == null || list.isEmpty()){
			buttonSaveVat = new JButton("Save");
			buttonSaveVat.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if(textField.getText() != ""){
						vatNumber = new VAT(Double.valueOf(textField.getText()),list.get(0).getVatId());
						vATDAO.insert(vatNumber);
					}
				}
			});
			buttonSaveVat.setBounds(255, 39, 89, 23);
			getContentPane().add(buttonSaveVat);
		} else{
			buttonModifyVat = new JButton("Modify");
			buttonModifyVat.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if(textField.getText() != ""){
						vatNumber = new VAT(Double.parseDouble(textField.getText()),list.get(0).getVatId());	
						vATDAO.update(vatNumber);
					}
				}
			});
			buttonModifyVat.setBounds(252, 39, 89, 23);
			getContentPane().add(buttonModifyVat);
			textField.setText(String.format("%4.0f",(list.get(0).getVat()-1)*100));
		}
	}
}

