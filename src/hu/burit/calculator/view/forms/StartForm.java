package hu.burit.calculator.view.forms;

import hu.burit.calculator.model.basket.Basket;
import hu.burit.calculator.model.dao.ProductDAO;
import hu.burit.calculator.model.dao.WholesaleDiscountDAO;
import hu.burit.calculator.model.databaseconnection.DatabaseConnection;
import hu.burit.calculator.model.product.Gravel;
import hu.burit.calculator.model.product.Product;
import hu.burit.calculator.model.utils.WholesaleDiscount;
import hu.burit.calculator.view.indicator.Flag;

import java.awt.EventQueue;
import java.awt.Frame;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.AbstractListModel;
import javax.swing.JInternalFrame;
import javax.swing.JSpinner;
import javax.swing.JRadioButton;
import javax.swing.JLabel;
import javax.swing.SpinnerNumberModel;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.Connection;
import java.util.List;
import java.util.ArrayList;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.JList;

import hu.burit.calculator.view.formparts.ProductsRenderer;
import hu.burit.calculator.view.formparts.SumModel;
import hu.burit.calculator.view.formparts.SumRenderer;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;



import javax.swing.JLayeredPane;


/**
 * @author <b>Tamas Buri, 
 * tamasburi.jr@gmail.com, 
 * 0036/70-555-777-5</b>
 */
@SuppressWarnings("serial")
public class StartForm extends JFrame {
	private JPanel contentPane;
	final JRadioButton radioButtonHorizontalWaterproofLayer;
	final JRadioButton radioButtonVerticalWaterproofLayer;
	final JRadioButton radioButtonHorizontalSurface;
	final JRadioButton radioButtonVerticalSurface;
	final JSpinner spinnerHorizontalSurface;
	final JSpinner spinnerVerticalSurface;
	final JRadioButton radioButtonRetail;
	final private JRadioButton radioButtonWholesale;
	private double productNetWholesalePrice;
	private VATForm vATForm;
	private JInternalFrame internalFrame;
	@SuppressWarnings("rawtypes")
	final JComboBox comboBoxHorizontalSurface;
	@SuppressWarnings("rawtypes")
	final JComboBox comboBoxVerticalSurface;
	
	private Product SIL04;
	private Product VA01;
	private Product SAD22Vertical;
	private Product EAA_A;
	private Product EAA_B;
	private Product SAD22Horizontal;
	
	private Gravel SGravelHorizontal;
	private Gravel SGravelVertical;
	private Gravel SCGravelHorizontal;
	private Gravel SCGravelVertical;
	private Gravel SDGravelHorizontal;
	private Gravel SDGravelVertical;
	private Gravel SMGravelHorizontal;
	private Gravel SMGravelVertical;
	private Gravel SFGravelHorizontal;
	private Gravel SFGravelVertical;
	@SuppressWarnings("rawtypes")
	private JList list;
	@SuppressWarnings("rawtypes")
	private JList list_1;
	private Basket basket;
	private JButton buttonRecalculate;
	private JMenuBar menuBar;
	private JMenu settingsMenu;
	private JMenuItem menuItemProducts;
	private JLabel labelSheathedSurface;
	private JLabel lblNewLabel;
	private JButton buttonCalculate;
	private JLabel labelQuantity;
	private JLabel labelNet;
	private JLabel labelGross;
	private JLabel labelSquareMeter;
	private JLabel labelProductName;
	private JMenuItem mntmfa;
	public static JButton buttonShow;
	private Connection sqlConnection;	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StartForm frame = new StartForm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public StartForm() {
		//we connect to the database
		sqlConnection = DatabaseConnection.getInstance().getConn();
		
		basket = new Basket();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setExtendedState(Frame.MAXIMIZED_BOTH);
		setBounds(100, 100, 837, 496);
		
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		settingsMenu = new JMenu("Settings");
		menuBar.add(settingsMenu);
		
		menuItemProducts = new JMenuItem("Products");
		
		settingsMenu.add(menuItemProducts);
		
		JMenuItem menuItemWholesaleDiscount = new JMenuItem("Wholesale discount");
		menuItemWholesaleDiscount.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				//we call this method to clear the basket and the screen
				deleteElements();
				WholesaleDiscountForm wholesaleDiscountForm = new WholesaleDiscountForm();
				getContentPane().add(wholesaleDiscountForm);
				wholesaleDiscountForm.show();
				wholesaleDiscountForm.setClosable(true);
				
				Flag.setWholesaleForm(true);
				Flag.setVatForm(false);
				setVisibleFalseOrTrue();
			}
		});
		settingsMenu.add(menuItemWholesaleDiscount);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(6, 6, 6, 6));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		list_1 = new JList();
		list_1.setBounds(22, 409, 799, 23);
		contentPane.add(list_1);
		
		//set Model
		list_1.setModel(new SumModel());
		list_1.setCellRenderer(new SumRenderer());
		
		
		JLayeredPane layeredPane = new JLayeredPane();
		layeredPane.setBounds(594, 35, 1, 1);
		contentPane.add(layeredPane);
		
		labelGross = new JLabel("Gross");
		labelGross.setBounds(530, 160, 166, 22);
		contentPane.add(labelGross);
				
		comboBoxVerticalSurface = new JComboBox();
		comboBoxVerticalSurface.setModel(new DefaultComboBoxModel(new String[] { "S", "SC", "SD", "SM", "SF" }));
		comboBoxVerticalSurface.setBounds(193, 62, 44, 20);
		contentPane.add(comboBoxVerticalSurface);
		
		radioButtonHorizontalWaterproofLayer = new JRadioButton("Waterproof layer");
		radioButtonHorizontalWaterproofLayer.setBounds(388, 35, 136, 23);
		contentPane.add(radioButtonHorizontalWaterproofLayer);
		
		comboBoxHorizontalSurface = new JComboBox();
		comboBoxHorizontalSurface.setModel(new DefaultComboBoxModel(new String[] { "S", "SC", "SD", "SM", "SF" }));
		comboBoxHorizontalSurface.setBounds(193, 36, 44, 20);
		
		contentPane.add(comboBoxHorizontalSurface);
				
		spinnerVerticalSurface = new JSpinner();
		spinnerVerticalSurface.setModel(new SpinnerNumberModel(new Double(0), new Double(0), null, new Double(0.1)));
		spinnerVerticalSurface.setBounds(285, 62, 60, 20);
		contentPane.add(spinnerVerticalSurface);
		radioButtonWholesale = new JRadioButton("Wholesale price");
		
		radioButtonWholesale.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if (radioButtonWholesale.getSelectedObjects() != null) {
					radioButtonRetail.setSelected(false);
				} else {
					radioButtonWholesale.setSelected(false);
					radioButtonRetail.setSelected(true);
				}
			}
		});
		radioButtonWholesale.setBounds(705, 35, 146, 23);
		contentPane.add(radioButtonWholesale);
						
		labelSheathedSurface = new JLabel("Sheathed surface");
		labelSheathedSurface.setBounds(21, 11, 157, 14);
		contentPane.add(labelSheathedSurface);
		
		labelQuantity = new JLabel("Quantity");
		labelQuantity.setBounds(191, 160, 160, 22);
		contentPane.add(labelQuantity);
		
		radioButtonVerticalWaterproofLayer = new JRadioButton("Waterproof layer");
		radioButtonVerticalWaterproofLayer.setBounds(388, 61, 136, 23);
		contentPane.add(radioButtonVerticalWaterproofLayer);
		
		spinnerHorizontalSurface = new JSpinner();
		spinnerHorizontalSurface.setModel(new SpinnerNumberModel(new Double(0), new Double(0), null, new Double(0.1)));
		spinnerHorizontalSurface.setBounds(285, 36, 60, 20);
		contentPane.add(spinnerHorizontalSurface);
										
		labelNet = new JLabel("Net");
		labelNet.setBounds(364, 160, 160, 22);
		contentPane.add(labelNet);
		
		radioButtonRetail = new JRadioButton("Retail price");
		
		radioButtonRetail.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				if (radioButtonRetail.getSelectedObjects() != null) {
					radioButtonWholesale.setSelected(false);
				} else {
					radioButtonRetail.setSelected(false);
					radioButtonWholesale.setSelected(true);
				}
			}
		});
		radioButtonRetail.setBounds(567, 35, 100, 23);
		radioButtonRetail.setSelected(true);
		contentPane.add(radioButtonRetail);
		
		buttonCalculate = new JButton("Calculate");
		buttonCalculate.setBounds(21, 105, 800, 25);
		contentPane.add(buttonCalculate);
		
		labelProductName = new JLabel("Product's name");
		labelProductName.setBounds(31, 160, 160, 22);
		contentPane.add(labelProductName);
		
		buttonRecalculate = new JButton("Recalculate");
		buttonRecalculate.setVisible(false);
		buttonRecalculate.addActionListener(new ActionListener() {
			
		public void actionPerformed(ActionEvent arg0) {
			//we call this method to clear the basket and the screen
			deleteElements();
			buttonCalculate.doClick();
			buttonRecalculate.setText("Recalculate");
			}
		});
		buttonRecalculate.setBounds(21, 106, 800, 23);
		contentPane.add(buttonRecalculate);
		
		radioButtonVerticalSurface = new JRadioButton("Vertical surface");
		radioButtonVerticalSurface.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (radioButtonHorizontalSurface.getSelectedObjects() == null	&& radioButtonVerticalSurface.getSelectedObjects() == null) {
					radioButtonHorizontalSurface.setSelected(true);
				}
			}
		});
		
		radioButtonVerticalSurface.setBounds(16, 61, 166, 23);
		contentPane.add(radioButtonVerticalSurface);
		
		radioButtonHorizontalSurface = new JRadioButton("Horizontal surface");
		radioButtonHorizontalSurface.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (radioButtonVerticalSurface.getSelectedObjects() == null
						&& radioButtonHorizontalSurface.getSelectedObjects() == null) {
					radioButtonVerticalSurface.setSelected(true);
				}
			}
		});
		
		radioButtonHorizontalSurface.setSelected(true);
		radioButtonHorizontalSurface.setBounds(16, 35, 166, 23);
				
		contentPane.add(radioButtonHorizontalSurface);
						
		lblNewLabel = new JLabel("Color code");
		lblNewLabel.setBounds(193, 11, 63, 14);
		contentPane.add(lblNewLabel);
						
		labelSquareMeter = new JLabel("Square meter");
		labelSquareMeter.setBounds(285, 11, 89, 14);
		contentPane.add(labelSquareMeter);
		
		list = new JList();
		list.setBounds(21, 193, 800, 183);
		contentPane.add(list);
		
		//set Model
		list.setModel(new MyModel());
		list.setCellRenderer(new ProductsRenderer());
		
		buttonShow = new JButton("Show");
		buttonShow.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				setVisibleFalseOrTrue();
			}
		});
		buttonShow.setBounds(789, 58, 89, 23);
		contentPane.add(buttonShow);
		buttonShow.setVisible(false);
		
		menuItemProducts.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				deleteElements();
				ProductsForm productsForm = new ProductsForm();
				getContentPane().add(productsForm);
								
				Flag.setProductsForm(true);
				Flag.setWholesaleForm(false);
				Flag.setVatForm(false);
				setVisibleFalseOrTrue();
				if(internalFrame == null){
					settingJInternalFrame();
				}
			}
		});			
		
		//we calculate the price of the materials we need
		buttonCalculate.addActionListener(new ActionListener() {		
		public void actionPerformed(ActionEvent arg0) {
			basket.numberOfProductsInBasket();
				
			buttonRecalculate.setVisible(true);
			if (radioButtonHorizontalSurface.getSelectedObjects() != null || radioButtonVerticalSurface.getSelectedObjects() != null) {
				// in case horizontal surface
				if (radioButtonHorizontalSurface.getSelectedObjects() != null	&& radioButtonVerticalSurface.getSelectedObjects() == null) {
					getShowHorizontalSurface(0, Double.valueOf(spinnerHorizontalSurface.getValue().toString()));
						
					//you can choose between retail price and wholesale price
					basket.getRetailOrWholesaleTotalPriceProductsFromBasket(basket);
					((SumModel)list_1.getModel()).insertElement(basket);

					// in case vertical surface
				} else if(radioButtonVerticalSurface.getSelectedObjects() != null && radioButtonHorizontalSurface.getSelectedObjects() == null) {
					getShowVerticalSurface(Double.valueOf(spinnerVerticalSurface.getValue().toString()), 0);	
						
					//you can choose between retail price and wholesale price
					basket.getRetailOrWholesaleTotalPriceProductsFromBasket(basket);
					((SumModel)list_1.getModel()).insertElement(basket);

					// in case horizontal and vertical surface
				} else if(radioButtonHorizontalSurface.getSelectedObjects() != null	&& radioButtonVerticalSurface.getSelectedObjects() != null) {
					getShowVerticalSurfaceAndHorizontalSurface(Double.valueOf(spinnerVerticalSurface.getValue().toString()),
					Double.valueOf(spinnerHorizontalSurface.getValue().toString()));
					//you can choose between retail price and wholesale price
					basket.getRetailOrWholesaleTotalPriceProductsFromBasket(basket);
					((SumModel)list_1.getModel()).insertElement(basket);
				}
					buttonCalculate.setVisible(false);
			}
		}
	});
		
	//we initialize the VAT form
	if(vATForm == null){
		Flag.setVatForm(false);
		Flag.setWholesaleForm(false);
		vATForm = new VATForm();
		mntmfa = new JMenuItem("VAT");
		mntmfa.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				//we call this method to clear the basket and the screen
				deleteElements();
				getContentPane().add(vATForm);
				vATForm.show();
				vATForm.setClosable(true);
				Flag.setVatForm(true);
				setVisibleFalseOrTrue();
			}
		});
	}
	settingsMenu.add(mntmfa);		
	}
	
	
	/**
	 * inner class
	 */
	@SuppressWarnings({ "rawtypes" })
	public class MyModel extends AbstractListModel{

		List<Product> productList = new ArrayList<Product>();
		
		@Override
		public Object getElementAt(int index) {
			return productList.get(index);
		}

		@Override
		public int getSize() {
			return productList.size();
		}
		
		public void insertElement(Product s){
			productList.add(s);
			this.fireContentsChanged(productList, 0, productList.size());
		}
		
		public void deleteElement(Product s){
			productList.remove(s);
		}		
	}
	
	/**
	 * this method hide the unnecessary elements
	 */
	public void setVisibleFalseOrTrue(){
		
		if(Flag.isVatForm() || Flag.isWholesaleForm() || Flag.isProductsForm()){
			StartForm.buttonShow.setVisible(true);
			radioButtonHorizontalWaterproofLayer.setVisible(false);
			radioButtonVerticalWaterproofLayer.setVisible(false);
			radioButtonHorizontalSurface.setVisible(false);
			radioButtonVerticalSurface.setVisible(false);
			spinnerHorizontalSurface.setVisible(false);
			spinnerVerticalSurface.setVisible(false);
			radioButtonRetail.setVisible(false);
			radioButtonWholesale.setVisible(false);
			
			comboBoxHorizontalSurface.setVisible(false);
			comboBoxVerticalSurface.setVisible(false);
			list.setVisible(false);
			list_1.setVisible(false);
			buttonRecalculate.setVisible(false);
			labelSheathedSurface.setVisible(false);
			lblNewLabel.setVisible(false);
			buttonCalculate.setVisible(false);
			labelQuantity.setVisible(false);
			labelNet.setVisible(false);
			labelGross.setVisible(false);
			labelProductName.setVisible(false);
			labelSquareMeter.setVisible(false);
		}else{
			StartForm.buttonShow.setVisible(false);
			radioButtonHorizontalWaterproofLayer.setVisible(true);
			radioButtonVerticalWaterproofLayer.setVisible(true);
			radioButtonHorizontalSurface.setVisible(true);
			radioButtonVerticalSurface.setVisible(true);
			spinnerHorizontalSurface.setVisible(true);
			spinnerVerticalSurface.setVisible(true);
			radioButtonRetail.setVisible(true);
			radioButtonWholesale.setVisible(true);
			
			comboBoxHorizontalSurface.setVisible(true);
			comboBoxVerticalSurface.setVisible(true);
			list.setVisible(true);
			list_1.setVisible(true);
			buttonRecalculate.setVisible(true);
			labelSheathedSurface.setVisible(true);
			lblNewLabel.setVisible(true);
			buttonCalculate.setVisible(true);
			labelQuantity.setVisible(true);
			labelNet.setVisible(true);
			labelGross.setVisible(true);
			labelProductName.setVisible(true);
			labelSquareMeter.setVisible(true);
		}
	}

	/**
	 * set the JInternal frame
	 */
	public void settingJInternalFrame(){
		ProductsForm f = new ProductsForm();
		f.setBounds(0, 0, 900, 300);
		f.setClosable(true);
		getContentPane().add(f);
		f.show();
		setVisibleFalseOrTrue();
	}
	
	/**
	 * we clear the basket and the screen
	 */
	public void deleteElements(){
		((MyModel)list.getModel()).deleteElement(SIL04);
		((MyModel)list.getModel()).deleteElement(VA01);
		((MyModel)list.getModel()).deleteElement(EAA_A);
		((MyModel)list.getModel()).deleteElement(EAA_B);
		((MyModel)list.getModel()).deleteElement(SAD22Vertical);
		((MyModel)list.getModel()).deleteElement(SAD22Horizontal);
		((MyModel)list.getModel()).deleteElement(SGravelHorizontal);
		((MyModel)list.getModel()).deleteElement(SGravelVertical);
		((MyModel)list.getModel()).deleteElement(SCGravelVertical);
		((MyModel)list.getModel()).deleteElement(SCGravelHorizontal);
		((MyModel)list.getModel()).deleteElement(SDGravelVertical);
		((MyModel)list.getModel()).deleteElement(SDGravelHorizontal);
		((MyModel)list.getModel()).deleteElement(SMGravelVertical);
		((MyModel)list.getModel()).deleteElement(SMGravelHorizontal);
		((MyModel)list.getModel()).deleteElement(SFGravelVertical);
		((MyModel)list.getModel()).deleteElement(SFGravelHorizontal);
	}
	
	/**
	 * we initialize the products
	 * @param squaremeterVertical we use this parameter if we get vertical surface
	 * @param squaremeterHorizontal we use this parameter if we get horizontal surface
	 */
	public void initProducts(double squaremeterVertical, double squaremeterHorizontal) {
		WholesaleDiscountDAO wholesaleDiscountDAO = new WholesaleDiscountDAO(sqlConnection);
		List<WholesaleDiscount> list = wholesaleDiscountDAO.findAll();
		double wholesaleDiscountPercent = 0;
		for (WholesaleDiscount wholesaleDiscount : list) {
			wholesaleDiscountPercent = wholesaleDiscount.getWholesaleDiscountPercent();
		}
		
		ProductDAO productDAO = new ProductDAO(sqlConnection);
		List<Product> productsList = productDAO.findAll();	
		for (int i = 0; i < productsList.size(); i++) {
			productNetWholesalePrice = productsList.get(i).getProductNetRetailSalePricePerKg()/wholesaleDiscountPercent;
			switch(productsList.get(i).getProductName()){
				case "Va01":
					VA01 = new Product(squaremeterVertical, productsList.get(i).getProductName().toString() , productsList.get(i).getProductNetRetailSalePricePerKg(), productNetWholesalePrice, productsList.get(i).getProductNetCostPriceEuro(), productsList.get(i).getProductConsumptionKgPerSquareMeter());
					break;
				case "Sil04":
					SIL04 = new Product(squaremeterVertical, productsList.get(i).getProductName().toString(), productsList.get(i).getProductNetRetailSalePricePerKg(), productNetWholesalePrice, productsList.get(i).getProductNetCostPriceEuro(), productsList.get(i).getProductConsumptionKgPerSquareMeter());
					break;
				case "Eaa/a":				
					EAA_A = new Product(squaremeterHorizontal, productsList.get(i).getProductName(), productsList.get(i).getProductNetRetailSalePricePerKg(), productNetWholesalePrice, productsList.get(i).getProductNetCostPriceEuro(), productsList.get(i).getProductConsumptionKgPerSquareMeter());
					break;
				case "Eaa/b":
					EAA_B = new Product(squaremeterHorizontal, productsList.get(i).getProductName(), productsList.get(i).getProductNetRetailSalePricePerKg(), productNetWholesalePrice, productsList.get(i).getProductNetCostPriceEuro(), productsList.get(i).getProductConsumptionKgPerSquareMeter());
					break;
				case "S gravel horizontal":
					SGravelHorizontal = new Gravel(squaremeterHorizontal, productsList.get(i).getProductName(), productsList.get(i).getProductNetRetailSalePricePerKg(), productNetWholesalePrice, productsList.get(i).getProductNetCostPriceEuro(), productsList.get(i).getProductConsumptionKgPerSquareMeter());
					break;
				case "S gravel vertical":
					SGravelVertical = new Gravel(squaremeterVertical, productsList.get(i).getProductName(), productsList.get(i).getProductNetRetailSalePricePerKg(), productNetWholesalePrice, productsList.get(i).getProductNetCostPriceEuro(), productsList.get(i).getProductConsumptionKgPerSquareMeter());
					break;
				case "SC gravel horizontal":
					SCGravelHorizontal = new Gravel(squaremeterHorizontal, productsList.get(i).getProductName(), productsList.get(i).getProductNetRetailSalePricePerKg(), productNetWholesalePrice, productsList.get(i).getProductNetCostPriceEuro(), productsList.get(i).getProductConsumptionKgPerSquareMeter());
					break;
				case "SC gravel vertical":
					SCGravelVertical = new Gravel(squaremeterVertical, productsList.get(i).getProductName(), productsList.get(i).getProductNetRetailSalePricePerKg(), productNetWholesalePrice, productsList.get(i).getProductNetCostPriceEuro(), productsList.get(i).getProductConsumptionKgPerSquareMeter());
					break;
				case "SD gravel horizontal":
					SDGravelHorizontal = new Gravel(squaremeterHorizontal, productsList.get(i).getProductName(), productsList.get(i).getProductNetRetailSalePricePerKg(), productNetWholesalePrice, productsList.get(i).getProductNetCostPriceEuro(), productsList.get(i).getProductConsumptionKgPerSquareMeter());
					break;
				case "SD gravel vertical":
					SDGravelVertical = new Gravel(squaremeterVertical, productsList.get(i).getProductName(), productsList.get(i).getProductNetRetailSalePricePerKg(), productNetWholesalePrice, productsList.get(i).getProductNetCostPriceEuro(), productsList.get(i).getProductConsumptionKgPerSquareMeter());
					break;
				case "SM gravel horizontal":
					SMGravelHorizontal = new Gravel(squaremeterHorizontal, productsList.get(i).getProductName(), productsList.get(i).getProductNetRetailSalePricePerKg(), productNetWholesalePrice, productsList.get(i).getProductNetCostPriceEuro(), productsList.get(i).getProductConsumptionKgPerSquareMeter());
					break;
				case "SM gravel vertical":
					SMGravelVertical = new Gravel(squaremeterVertical, productsList.get(i).getProductName(), productsList.get(i).getProductNetRetailSalePricePerKg(), productNetWholesalePrice, productsList.get(i).getProductNetCostPriceEuro(), productsList.get(i).getProductConsumptionKgPerSquareMeter());
					break;
				case "SF gravel horizontal":
					SFGravelHorizontal = new Gravel(squaremeterHorizontal, productsList.get(i).getProductName(), productsList.get(i).getProductNetRetailSalePricePerKg(), productNetWholesalePrice, productsList.get(i).getProductNetCostPriceEuro(), productsList.get(i).getProductConsumptionKgPerSquareMeter());
					break;	
				case "SF gravel vertical":
					SFGravelVertical = new Gravel(squaremeterVertical, productsList.get(i).getProductName(), productsList.get(i).getProductNetRetailSalePricePerKg(), productNetWholesalePrice, productsList.get(i).getProductNetCostPriceEuro(), productsList.get(i).getProductConsumptionKgPerSquareMeter());
					break;
				case "Sad22 vertical":
					SAD22Vertical = new Product(squaremeterVertical, productsList.get(i).getProductName(), productsList.get(i).getProductNetRetailSalePricePerKg(), productNetWholesalePrice, productsList.get(i).getProductNetCostPriceEuro(), productsList.get(i).getProductConsumptionKgPerSquareMeter());
					break;
				case "Sad22 horizontal":
					SAD22Horizontal = new Product(squaremeterHorizontal, productsList.get(i).getProductName(), productsList.get(i).getProductNetRetailSalePricePerKg(), productNetWholesalePrice, productsList.get(i).getProductNetCostPriceEuro(), productsList.get(i).getProductConsumptionKgPerSquareMeter());
					break;
			}
		}
	}

	/**
	 * If the user select retail price and horizontal surface we add products to the basket and to the view
	 */
	public void comboBoxSelectionRetailPriceHorizontal() {
		Flag.setRetailRadioButton(true);
		Flag.setWholesaleRadioButton(false);
		basket.addProductsToBasket(EAA_A);
		basket.addProductsToBasket(EAA_B);
		((MyModel)list.getModel()).insertElement(EAA_A);
		((MyModel)list.getModel()).insertElement(EAA_B);
	
		// add horizontal surface to the view and to the basket
		if (comboBoxHorizontalSurface.getSelectedItem() == "S") {
			((MyModel)list.getModel()).insertElement(SGravelHorizontal);
			basket.addProductsToBasket(SGravelHorizontal);
		} else if (comboBoxHorizontalSurface.getSelectedItem() == "SC") {
			((MyModel)list.getModel()).insertElement(SCGravelHorizontal);
			basket.addProductsToBasket(SCGravelHorizontal);
		} else if (comboBoxHorizontalSurface.getSelectedItem() == "SD") {
			((MyModel)list.getModel()).insertElement(SDGravelHorizontal);
			basket.addProductsToBasket(SDGravelHorizontal);
		} else if (comboBoxHorizontalSurface.getSelectedItem() == "SM") {
			((MyModel)list.getModel()).insertElement(SMGravelHorizontal);
			basket.addProductsToBasket(SMGravelHorizontal);
		} else if (comboBoxHorizontalSurface.getSelectedItem() == "SF") {
			((MyModel)list.getModel()).insertElement(SFGravelHorizontal);
			basket.addProductsToBasket(SFGravelHorizontal);
		}
	}

	/**
	 *  If the user select retail price and vertical surface we add products to the basket and to the view
	 */
	public void comboBoxSelectionRetailPriceVertical() {
		Flag.setRetailRadioButton(true);
		Flag.setWholesaleRadioButton(false);
		((MyModel)list.getModel()).insertElement(SIL04);
		((MyModel)list.getModel()).insertElement(VA01);
		((SumModel)list_1.getModel()).insertElement(basket);
			
		basket.addProductsToBasket(SIL04);
		basket.addProductsToBasket(VA01);

		// add vertical surface to the view and to the basket
		if (comboBoxVerticalSurface.getSelectedItem() == "S") {
			((MyModel)list.getModel()).insertElement(SGravelVertical);
			basket.addProductsToBasket(SGravelVertical);
		} else if (comboBoxVerticalSurface.getSelectedItem() == "SC") {
			((MyModel)list.getModel()).insertElement(SCGravelVertical);
			basket.addProductsToBasket(SCGravelVertical);
		} else if (comboBoxVerticalSurface.getSelectedItem() == "SD") {
			((MyModel)list.getModel()).insertElement(SDGravelVertical);
			basket.addProductsToBasket(SDGravelVertical);
		} else if (comboBoxVerticalSurface.getSelectedItem() == "SM") {
			((MyModel)list.getModel()).insertElement(SMGravelVertical);
			basket.addProductsToBasket(SMGravelVertical);
		} else if (comboBoxVerticalSurface.getSelectedItem() == "SF") {
			((MyModel)list.getModel()).insertElement(SFGravelVertical);
			basket.addProductsToBasket(SFGravelVertical);
		}
	}

	/**
	 * If the user select wholesale price and horizontal surface we add products to the basket and to the view
	 */
	public void comboBoxSelectionWholesalePriceHorizontal() {
		Flag.setRetailRadioButton(false);
		Flag.setWholesaleRadioButton(true);
		basket.addProductsToBasket(EAA_A);
		basket.addProductsToBasket(EAA_B);
		((MyModel)list.getModel()).insertElement(EAA_A);
		((MyModel)list.getModel()).insertElement(EAA_B);
		
		((SumModel)list_1.getModel()).insertElement(basket);

		// add horizontal surface to the view and to the basket
		if (comboBoxHorizontalSurface.getSelectedItem() == "S") {
			((MyModel)list.getModel()).insertElement(SGravelHorizontal);
			basket.addProductsToBasket(SGravelHorizontal);
		} else if (comboBoxHorizontalSurface.getSelectedItem() == "SC") {
			((MyModel)list.getModel()).insertElement(SCGravelHorizontal);
			basket.addProductsToBasket(SCGravelHorizontal);
		} else if (comboBoxHorizontalSurface.getSelectedItem() == "SD") {
			((MyModel)list.getModel()).insertElement(SDGravelHorizontal);
			basket.addProductsToBasket(SDGravelHorizontal);
		} else if (comboBoxHorizontalSurface.getSelectedItem() == "SM") {
			((MyModel)list.getModel()).insertElement(SMGravelHorizontal);
			basket.addProductsToBasket(SMGravelHorizontal);
		} else if (comboBoxHorizontalSurface.getSelectedItem() == "SF") {
			((MyModel)list.getModel()).insertElement(SFGravelHorizontal);
			basket.addProductsToBasket(SFGravelHorizontal);
		}
	}

	/**
	 * If the user select wholesale price and vertical surface we add products to the basket and to the view
	 */
	public void comboBoxSelectionWholesalePriceVertical() {
		Flag.setRetailRadioButton(false);
		Flag.setWholesaleRadioButton(true);
		((MyModel)list.getModel()).insertElement(SIL04);
		((MyModel)list.getModel()).insertElement(VA01);
		((SumModel)list_1.getModel()).insertElement(basket);
			
		basket.addProductsToBasket(SIL04);
		basket.addProductsToBasket(VA01);
		
		// add vertical surface to the view and to the basket
		if (comboBoxVerticalSurface.getSelectedItem() == "S") {
			((MyModel)list.getModel()).insertElement(SGravelVertical);
			basket.addProductsToBasket(SGravelVertical);
		} else if (comboBoxVerticalSurface.getSelectedItem() == "SC") {
			((MyModel)list.getModel()).insertElement(SCGravelVertical);
			basket.addProductsToBasket(SCGravelVertical);
		} else if (comboBoxVerticalSurface.getSelectedItem() == "SD") {
			((MyModel)list.getModel()).insertElement(SDGravelVertical);
			basket.addProductsToBasket(SDGravelVertical);
		} else if (comboBoxVerticalSurface.getSelectedItem() == "SM") {
			((MyModel)list.getModel()).insertElement(SMGravelVertical);
			basket.addProductsToBasket(SMGravelVertical);
		} else if (comboBoxVerticalSurface.getSelectedItem() == "SF") {
			((MyModel)list.getModel()).insertElement(SFGravelVertical);
			basket.addProductsToBasket(SFGravelVertical);
		}
	}
	
	/**
	 * Let's show all the products if the user select horizontal surface 
	 * @param squaremeterVertical we must set this value to 0 because we get horizontal surface
	 * @param squaremeterHorizontal we add how many square meters of horizontal surface we have
	 */
	public void getShowHorizontalSurface(double squaremeterVertical,double squaremeterHorizontal) {
		initProducts(squaremeterVertical, squaremeterHorizontal);

		if (radioButtonRetail.getSelectedObjects() != null) {
			comboBoxSelectionRetailPriceHorizontal();

			// waterproof layer is selected 
			if (radioButtonHorizontalWaterproofLayer.getSelectedObjects() != null) {
				//this flag indicates that we were selected the horizontal waterproof layer
				Flag.setHorizontalWaterproofLayerRadioButton(true);
				((MyModel)list.getModel()).insertElement(SAD22Horizontal);
				//you add SAD22 to basket
				basket.addProductsToBasket(SAD22Horizontal);		
			} 
		} else {
			comboBoxSelectionWholesalePriceHorizontal();

			if (radioButtonHorizontalWaterproofLayer.getSelectedObjects() != null) {
				//this flag indicates that we were selected the horizontal waterproof layer
				Flag.setHorizontalWaterproofLayerRadioButton(true);
				((MyModel)list.getModel()).insertElement(SAD22Horizontal);
				//you add SAD22 to basket
				basket.addProductsToBasket(SAD22Horizontal);	
			}
		}
	}

	/**
	 * Let's show all the products if the user select vertical surface 
	 * @param squaremeterVertical we add how many square meters of vertical surface we have
	 * @param squaremeterHorizontal we must set this value to 0 because we get vertical surface
	 */
	public void getShowVerticalSurface(double squaremeterVertical,double squaremeterHorizontal) {
		initProducts(squaremeterVertical, squaremeterHorizontal);

		if (radioButtonRetail.getSelectedObjects() != null) {
			radioButtonWholesale.setSelected(false);
			comboBoxSelectionRetailPriceVertical();
			
			if (radioButtonVerticalWaterproofLayer.getSelectedObjects() != null) {
				//this flag indicates that we were selected the vertical waterproof layer
				Flag.setVerticalWaterproofLayerRadioButton(true);
				((MyModel)list.getModel()).insertElement(SAD22Vertical);
				//you add SAD22 to basket
				basket.addProductsToBasket(SAD22Vertical);
			} 
		} else {
			comboBoxSelectionWholesalePriceVertical();
			
			if (radioButtonVerticalWaterproofLayer.getSelectedObjects() != null) {
				//this flag indicates that we were selected the vertical waterproof layer
				Flag.setVerticalWaterproofLayerRadioButton(true);
				((MyModel)list.getModel()).insertElement(SAD22Vertical);
				//you add SAD22 to basket
				basket.addProductsToBasket(SAD22Vertical);
			}
		}
	}
	
	/**
	 * 
	 * @param squaremeterVertical we add how many square meters of vertical surface we have
	 * @param squaremeterHorizontal we add how many square meters of horizontal surface we have
	 */
	public void getShowVerticalSurfaceAndHorizontalSurface(double squaremeterVertical,double squaremeterHorizontal) {
		initProducts(squaremeterVertical, squaremeterHorizontal);
		
		if (radioButtonRetail.getSelectedObjects() != null) {
			Flag.setRetailRadioButton(true);
			Flag.setWholesaleRadioButton(false);
			radioButtonWholesale.setSelected(false);
			comboBoxSelectionRetailPriceHorizontal();
			comboBoxSelectionRetailPriceVertical();
			
			// in vertical waterproof layer case
			if (radioButtonVerticalWaterproofLayer.getSelectedObjects() != null && radioButtonHorizontalWaterproofLayer.getSelectedObjects() == null) {
				//this flag indicates that we were selected the vertical waterproof layer
				Flag.setVerticalWaterproofLayerRadioButton(true);
				((MyModel)list.getModel()).insertElement(SAD22Vertical);
				//you add SAD22 to basket
				basket.addProductsToBasket(SAD22Vertical);
			// in horizontal waterproof layer case
			} else if (radioButtonHorizontalWaterproofLayer.getSelectedObjects() != null && radioButtonVerticalWaterproofLayer.getSelectedObjects() == null) {
				//this flag indicates that we were selected the horizontal waterproof layer
				Flag.setHorizontalWaterproofLayerRadioButton(true);
				((MyModel)list.getModel()).insertElement(SAD22Horizontal);
				//you add SAD22 to basket
				basket.addProductsToBasket(SAD22Horizontal);
			// in horizontal and vertical waterproof layer case
			} else if (radioButtonHorizontalWaterproofLayer.getSelectedObjects() != null && radioButtonVerticalWaterproofLayer.getSelectedObjects() != null) {
				//this flag indicates that we were selected the vertical waterproof layer
				Flag.setVerticalWaterproofLayerRadioButton(true);
				((MyModel)list.getModel()).insertElement(SAD22Vertical);
				//you add SAD22 to basket
				basket.addProductsToBasket(SAD22Vertical);
				
				//this flag indicates that we were selected the horizontal waterproof layer
				Flag.setHorizontalWaterproofLayerRadioButton(true);
				((MyModel)list.getModel()).insertElement(SAD22Horizontal);
				//you add SAD22 to basket
				basket.addProductsToBasket(SAD22Horizontal);
			}
		} else {
			Flag.setRetailRadioButton(false);
			Flag.setWholesaleRadioButton(true);
			comboBoxSelectionWholesalePriceHorizontal();
			comboBoxSelectionWholesalePriceVertical();
			
			// in vertical waterproof layer case
			if (radioButtonVerticalWaterproofLayer.getSelectedObjects() != null && radioButtonHorizontalWaterproofLayer.getSelectedObjects() == null) {
				//this flag indicates that we were selected the vertical waterproof layer
				Flag.setVerticalWaterproofLayerRadioButton(true);
				((MyModel)list.getModel()).insertElement(SAD22Vertical);
				//you add SAD22 to basket
				basket.addProductsToBasket(SAD22Vertical);
			// in horizontal waterproof layer case
			} else if (radioButtonHorizontalWaterproofLayer.getSelectedObjects() != null && radioButtonVerticalWaterproofLayer.getSelectedObjects() == null) {
				//this flag indicates that we were selected the horizontal waterproof layer
				Flag.setHorizontalWaterproofLayerRadioButton(true);
				((MyModel)list.getModel()).insertElement(SAD22Horizontal);
				//you add SAD22 to basket
				basket.addProductsToBasket(SAD22Horizontal);
			// in horizontal and vertical waterproof layer case
			} else if (radioButtonHorizontalWaterproofLayer.getSelectedObjects() != null && radioButtonVerticalWaterproofLayer.getSelectedObjects() != null) {
				//this flag indicates that we were selected the vertical waterproof layer
				Flag.setVerticalWaterproofLayerRadioButton(true);
				((MyModel)list.getModel()).insertElement(SAD22Vertical);
				//you add SAD22 to basket
				basket.addProductsToBasket(SAD22Vertical);
				
				//this flag indicates that we were selected the horizontal waterproof layer
				Flag.setHorizontalWaterproofLayerRadioButton(true);
				((MyModel)list.getModel()).insertElement(SAD22Horizontal);
				//you add SAD22 to basket
				basket.addProductsToBasket(SAD22Horizontal);
			} 
		}
	}
}
