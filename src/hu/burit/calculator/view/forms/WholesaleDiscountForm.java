package hu.burit.calculator.view.forms;

import hu.burit.calculator.model.dao.WholesaleDiscountDAO;
import hu.burit.calculator.model.databaseconnection.DatabaseConnection;
import hu.burit.calculator.model.utils.WholesaleDiscount;
import hu.burit.calculator.view.indicator.Flag;

import java.awt.EventQueue;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

/**
 * @author <b>Tamas Buri, 
 * tamasburi.jr@gmail.com, 
 * 0036/70-555-777-5</b>
 */
public class WholesaleDiscountForm extends JInternalFrame {
	private static final long serialVersionUID = 1L;
	private JTextField textField;
	private JButton buttonSaveWholesaleDiscount;
	private JButton closeButton;
	private JButton modifyButton;
	private WholesaleDiscount wholesaleDiscount;
	private WholesaleDiscountDAO wholesaleDiscountDAO;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					WholesaleDiscountForm frame = new WholesaleDiscountForm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the WholesaleDiscount frame.
	 */
	public WholesaleDiscountForm() {
		setBounds(100, 100, 523, 136);
		getContentPane().setLayout(null);
		
		JLabel label = new JLabel("Wholesale discount in %");
		label.setBounds(37, 45, 170, 14);
		getContentPane().add(label);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(238, 42, 86, 20);
		getContentPane().add(textField);
		
		//implements wholesaleDiscountDAO
		wholesaleDiscountDAO = new WholesaleDiscountDAO(DatabaseConnection.getInstance().getConn());
		List<WholesaleDiscount> list = wholesaleDiscountDAO.findAll();
		
		if(list == null || list.isEmpty()){
				buttonSaveWholesaleDiscount = new JButton("Save");
				buttonSaveWholesaleDiscount.addMouseListener(new MouseAdapter() {
					@Override
					public void mousePressed(MouseEvent arg0) {
						if(textField.getText() != ""){
							wholesaleDiscount = new WholesaleDiscount(Double.valueOf(textField.getText()));
							wholesaleDiscountDAO.insert(wholesaleDiscount);
						}
					}
				});
				buttonSaveWholesaleDiscount.setBounds(377, 41, 89, 23);
				getContentPane().add(buttonSaveWholesaleDiscount);
		}else{
			modifyButton = new JButton("Modify");
			modifyButton.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					if(textField.getText() != ""){
						wholesaleDiscount = new WholesaleDiscount(Double.valueOf(textField.getText()));
						wholesaleDiscountDAO.update(wholesaleDiscount);
					}
				}
			});
			modifyButton.setBounds(377, 41, 89, 23);
			getContentPane().add(modifyButton);
			textField.setText(String.format("%4.0f",(list.get(0).getWholesaleDiscountPercent()-1)*100));
		}
		
		closeButton = new JButton("Close");
		closeButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				Flag.setWholesaleForm(false);
				Flag.setVatForm(false);
				dispose();
			}
		});
		closeButton.setBounds(238, 73, 89, 23);
		getContentPane().add(closeButton);
		
		JLabel labelPercent = new JLabel("%");
		labelPercent.setBounds(334, 45, 22, 14);
		getContentPane().add(labelPercent);
	}
}
