package hu.burit.calculator.view.indicator;

/**
 * @author <b>Tamas Buri, 
 * tamasburi.jr@gmail.com, 
 * 0036/70-555-777-5</b>
 */
public class Flag {
	private static boolean wholesaleRadioButton = false;
	private static boolean retailRadioButton = true;
	private static boolean horizontalWaterproofLayerRadioButton = false;
	private static boolean verticalWaterproofLayerRadioButton = false;
	private static boolean vatForm = false;
	private static boolean wholesaleForm = false;
	private static boolean productsForm = false;
	
	/**
	 * private constructor
	 */
	private Flag(){}
	
	//getters and setters
	public static boolean isWholesaleRadioButton() {
		return wholesaleRadioButton;
	}
	public static void setWholesaleRadioButton(boolean wholesaleRadioButton) {
		Flag.wholesaleRadioButton = wholesaleRadioButton;
	}
	
	public static boolean isRetailRadioButton() {
		return retailRadioButton;
	}
	
	public static void setRetailRadioButton(boolean retailRadioButton) {
		Flag.retailRadioButton = retailRadioButton;
	}

	public static boolean isHorizontalWaterproofLayerRadioButton() {
		return horizontalWaterproofLayerRadioButton;
	}

	public static void setHorizontalWaterproofLayerRadioButton(boolean horizontalWaterproofLayerRadioButton) {
		Flag.horizontalWaterproofLayerRadioButton = horizontalWaterproofLayerRadioButton;
	}

	public static boolean isVerticalWaterproofLayerRadioButton() {
		return verticalWaterproofLayerRadioButton;
	}

	public static void setVerticalWaterproofLayerRadioButton(
			boolean verticalWaterproofLayerRadioButton) {
		Flag.verticalWaterproofLayerRadioButton = verticalWaterproofLayerRadioButton;
	}

	public static boolean isVatForm() {
		return vatForm;
	}

	public static void setVatForm(boolean vat) {
		Flag.vatForm = vat;
	}

	public static boolean isWholesaleForm() {
		return wholesaleForm;
	}

	public static void setWholesaleForm(boolean wholesaleForm) {
		Flag.wholesaleForm = wholesaleForm;
	}

	public static boolean isProductsForm() {
		return productsForm;
	}

	public static void setProductsForm(boolean productsForm) {
		Flag.productsForm = productsForm;
	}
	//end getters and setters
}
