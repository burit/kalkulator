package hu.burit.calculator.view.formparts;

import hu.burit.calculator.model.basket.Basket;

import java.util.List;
import java.util.ArrayList;
import javax.swing.AbstractListModel;

/**
 * @author <b>Tamas Buri, 
 * tamasburi.jr@gmail.com, 
 * 0036/70-555-777-5</b>
 */
@SuppressWarnings({ "rawtypes", "serial" })
public class SumModel extends AbstractListModel{

	List<Basket> itemsList = new ArrayList<Basket>();
	
	/**
	 * @return itemsList.get(index)
	 */
	@Override
	public Object getElementAt(int index) {
		return itemsList.get(index);
	}

	/**
	 * @return itemsList.size()
	 */
	@Override
	public int getSize() {
		return itemsList.size();
	}
	
	/**
	 * 
	 * @param s
	 */
	public void insertElement(Basket s){
		itemsList.add(s);
		this.fireContentsChanged(itemsList, 0, itemsList.size());
	}
	
	/**
	 * 
	 * @param s
	 */
	public void deleteElement(Basket s){
		itemsList.remove(s);
	}
}
