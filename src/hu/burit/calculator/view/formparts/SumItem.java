package hu.burit.calculator.view.formparts;

import hu.burit.calculator.model.basket.Basket;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

/**
 * @author <b>Tamas Buri, 
 * tamasburi.jr@gmail.com, 
 * 0036/70-555-777-5</b>
 */
public class SumItem extends JPanel {	
	private static final long serialVersionUID = 1L;
	private JLabel labelTotal;
	private JLabel labelKg;
	private JLabel labelNet;
	private JLabel labelGross;

	/**
	 * Create the panel.
	 * @param b
	 */
	public SumItem(Basket b) {
		initComponents();
		labelTotal.setText("Total");
		labelKg.setText(String.format("%4.2f kg", b.getTotalGoodsKg()));
		labelNet.setText(String.format("%4.0f Ft", b.getTotalGoodsNetPrice()));
		labelGross.setText(String.format("%4.0f Ft", b.getTotalGoodsGrossPrice()));
	}
	
	/**
	 * 
	 */
	public void initComponents(){
		setLayout(null);
		
		labelTotal = new JLabel("Total");
		labelTotal.setBounds(10, 0, 160, 22);
		add(labelTotal);
		
		labelKg = new JLabel("kg");
		labelKg.setBounds(120, 0, 55, 14);
		add(labelKg);
		
		labelNet = new JLabel("Net");
		labelNet.setBounds(228, 11, 55, 14);
		add(labelNet);
		
		labelGross = new JLabel("Gross");
		labelGross.setBounds(336, 11, 55, 14);
		add(labelGross);
		
        setOpaque(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        layout.setHorizontalGroup(
        	layout.createParallelGroup(Alignment.LEADING)
        		.addGroup(layout.createSequentialGroup()
        			.addContainerGap()
        			.addComponent(labelTotal, GroupLayout.PREFERRED_SIZE, 148, GroupLayout.PREFERRED_SIZE)
        			.addGap(18)
        			.addComponent(labelKg, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addComponent(labelNet, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addComponent(labelGross, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
        			.addContainerGap())
        );
        layout.setVerticalGroup(
        	layout.createParallelGroup(Alignment.LEADING)
        		.addGroup(layout.createParallelGroup(Alignment.BASELINE)
        			.addComponent(labelKg, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
        			.addComponent(labelNet, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
        			.addComponent(labelGross, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
        			.addComponent(labelTotal, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
        );
        this.setLayout(layout);
	}
}
