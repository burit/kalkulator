package hu.burit.calculator.view.formparts;

import hu.burit.calculator.model.product.Product;

import javax.swing.JList;
import javax.swing.ListCellRenderer;


import java.awt.Component;

/**
 * @author <b>Tamas Buri, 
 * tamasburi.jr@gmail.com, 
 * 0036/70-555-777-5</b>
 */
public class ProductsRenderer implements ListCellRenderer<Product> {

	/**
	 * @return p
	 */
	@Override
	public Component getListCellRendererComponent(
			JList<? extends Product> list, Product value, int index,
			boolean isSelected, boolean cellHasFocus) {

		ProductsItem p = new ProductsItem(value);
		if (isSelected) {
			p.setOpaque(true);
			p.setForeground(list.getSelectionForeground());
			p.setBackground(list.getSelectionBackground());
		} else {
			p.setOpaque(false);
			p.setForeground(list.getForeground());
			p.setBackground(list.getBackground());
		}
		return p;
	}
}
