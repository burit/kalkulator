package hu.burit.calculator.view.formparts;

import hu.burit.calculator.model.basket.Basket;

import java.awt.Component;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 * @author <b>Tamas Buri, 
 * tamasburi.jr@gmail.com, 
 * 0036/70-555-777-5</b>
 */
public class SumRenderer implements ListCellRenderer<Basket>{

	/**
	 * @param list
	 * @param value
	 * @param index
	 * @param isSelected
	 * @param cellHasFocus
	 * @return sumItem
	 */
	@Override
	public Component getListCellRendererComponent(
			JList<? extends Basket> list, Basket value, int index, boolean isSelected, boolean cellHasFocus) {
		SumItem sumItem = new SumItem(value);
		return sumItem;
	}
}

