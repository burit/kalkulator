package hu.burit.calculator.view.formparts;

import hu.burit.calculator.model.product.Product;
import hu.burit.calculator.view.indicator.Flag;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.LayoutStyle;


/**
 * @author <b>Tamas Buri, 
 * tamasburi.jr@gmail.com, 
 * 0036/70-555-777-5</b>
 */
@SuppressWarnings("serial")
public class ProductsItem extends javax.swing.JPanel {

	/**
	 * constructor
	 */
	public ProductsItem() {
		initComponents();
	}

	/**
	 * constructor with one parameter. We set all the value into the labels to see what we want
	 * @param d
	 */
	public ProductsItem(Product d) {

		initComponents();

		this.jLabel1.setText(d.getProductName());
		this.jLabel2.setText(String.format("%4.2f kg",
				d.getProductConsumptionTotalKg()));
		if (Flag.isWholesaleRadioButton()) {
			this.jLabel4.setText(String.format("%4.0f Ft",
					d.getProductNetWholesaleSalePriceTotal()));
			this.jLabel5.setText(String.format("%4.0f Ft",
					d.getProductGrossWholesaleSalePriceTotal()));
		} else {
			this.jLabel4.setText(String.format("%4.0f Ft",
					d.getProductNetRetailSalePriceTotal()));
			this.jLabel5.setText(String.format("%4.0f Ft",
					d.getProductGrossRetailSalePriceTotal()));
		}
	}

	/**
	 * 
	 */
	private void initComponents() {

		jLabel0 = new JLabel();
		jLabel1 = new JLabel();
		jLabel2 = new JLabel();
		jLabel4 = new JLabel();
		jLabel5 = new JLabel();

		setOpaque(false);

		GroupLayout layout = new GroupLayout(this);
		this.setLayout(layout);

		layout.setHorizontalGroup(layout
				.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(
						layout.createSequentialGroup()
								.addComponent(jLabel0,
										GroupLayout.PREFERRED_SIZE, 10,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(jLabel1,
										GroupLayout.PREFERRED_SIZE, 160,
										GroupLayout.PREFERRED_SIZE)

								.addComponent(jLabel2,
										GroupLayout.PREFERRED_SIZE, 160,
										GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(
										LayoutStyle.ComponentPlacement.RELATED)

								.addComponent(jLabel4,
										GroupLayout.PREFERRED_SIZE, 160,
										GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(
										LayoutStyle.ComponentPlacement.RELATED)

								.addPreferredGap(
										LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(jLabel5,
										GroupLayout.PREFERRED_SIZE, 160,
										GroupLayout.PREFERRED_SIZE)

								.addPreferredGap(
										LayoutStyle.ComponentPlacement.RELATED)));
		layout.setVerticalGroup(layout.createParallelGroup(
				GroupLayout.Alignment.LEADING).addGroup(
				layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
						.addComponent(jLabel0, GroupLayout.PREFERRED_SIZE, 22,
								GroupLayout.PREFERRED_SIZE)
						.addComponent(jLabel1, GroupLayout.PREFERRED_SIZE, 22,
								GroupLayout.PREFERRED_SIZE)
						.addComponent(jLabel2, GroupLayout.PREFERRED_SIZE, 22,
								GroupLayout.PREFERRED_SIZE)

						.addComponent(jLabel4, GroupLayout.PREFERRED_SIZE, 22,
								GroupLayout.PREFERRED_SIZE)

						.addComponent(jLabel5, GroupLayout.PREFERRED_SIZE, 22,
								GroupLayout.PREFERRED_SIZE)

		));
	}// </editor-fold>
		// Variables declaration - do not modify

	private JLabel jLabel0;
	private JLabel jLabel1;
	private JLabel jLabel2;
	private JLabel jLabel4;
	private JLabel jLabel5;
	// End of variables declaration
}
