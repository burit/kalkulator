package hu.burit.calculator.model.utils;

/**
 * @author <b>Tamas Buri, 
 * tamasburi.jr@gmail.com, 
 * 0036/70-555-777-5</b>
 */
public class VAT {
	
	private double vat;
	private int id;

	//getters and setters
	public double getVat() {
		return vat;
	}

	public void setVat(double vat) {
		this.vat = (vat/100)+1;
	}

	public int getVatId(){
		return id;
	}
	
	public void setVatId(int id){
		this.id = id;
	}
	//end getters and setters
	
	/**
	 * constructor with one parameter
	 * @param vat
	 */
	public VAT(double vat){
		this.vat = vat;
	}
	
	/**
	 * constructor with two parameter
	 * @param vat
	 * @param vatId
	 */
	public VAT(double vat,int vatId){
		this.vat = (vat/100)+1;
		this.id = vatId;
	}
}
