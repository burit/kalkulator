package hu.burit.calculator.model.utils;

/**
 * @author <b>Tamas Buri, 
 * tamasburi.jr@gmail.com, 
 * 0036/70-555-777-5</b>
 */
public class EuroExchangeRate {
	
	private double euroExchangeRate;

	//getters and setters
	public double getEuroExchangeRate() {
		return this.euroExchangeRate;
	}

	public void setEuroExchangeRate(double euroExchangeRate) {
		this.euroExchangeRate = euroExchangeRate;
	}
	//end getters and setters
}
