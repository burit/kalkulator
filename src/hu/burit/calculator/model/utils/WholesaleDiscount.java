package hu.burit.calculator.model.utils;

/**
 * @author <b>Tamas Buri, 
 * tamasburi.jr@gmail.com, 
 * 0036/70-555-777-5</b>
 */
public class WholesaleDiscount {

	private double wholesaleDiscount;

	//getter and setter
	public double getWholesaleDiscountPercent() {
		return wholesaleDiscount;
	}

	public void setWholesaleDiscountPercent(double wholesaleDiscount) {
		this.wholesaleDiscount = wholesaleDiscount;
	}
	//end getter and setter
	
	/**
	 * constructor with one parameter
	 * @param wholesaleDiscount
	 */
	public WholesaleDiscount(double wholesaleDiscount){
		this.wholesaleDiscount = (wholesaleDiscount/100)+1;
	}
}
