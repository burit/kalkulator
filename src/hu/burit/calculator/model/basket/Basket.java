package hu.burit.calculator.model.basket;

import hu.burit.calculator.model.product.Product;
import hu.burit.calculator.view.indicator.Flag;

import java.util.List;
import java.util.ArrayList;


/**
 * @author <b>Tamas Buri, 
 * tamasburi.jr@gmail.com, 
 * 0036/70-555-777-5</b>
 */
public class Basket {
	private double totalGoodsKg;
	private double totalGoodsNetPrice;
	private double totalGoodsGrossPrice;
	
	private List<Product> basket = new ArrayList<>();

	//getters and setters
	public double getTotalGoodsKg() {
		return totalGoodsKg;
	}

	public void setTotalGoodsKg(double totalGoodsKg) {
		this.totalGoodsKg = totalGoodsKg;
	}

	public double getTotalGoodsNetPrice() {
		return totalGoodsNetPrice;
	}

	public void setTotalGoodsNetPrice(double totalGoodsNet) {
		this.totalGoodsNetPrice = totalGoodsNet;
	}

	public double getTotalGoodsGrossPrice() {
		return totalGoodsGrossPrice;
	}

	public void setTotalGoodsGrossPrice(double totalGoodsGross) {
		this.totalGoodsGrossPrice = totalGoodsGross;
	}

	public List<Product> getBasket() {
		return basket;
	}

	public void setBasket(List<Product> basket) {
		this.basket = basket;
	}
	//end getters and setters
	
	
	/**
	 *  you can add products to basket
	 *  @param p Product you want to add to basket
	 */
	public void addProductsToBasket(Product p){
		getBasket().add(p);
		totalGoodsKgInBasket();
	}
	
	/**
	 * you can delete products from basket
	 * @param i this is the index of the Product you want to delete
	 */
	public void deleteProductsFromBasket(int i){
		getBasket().remove(i);
	}
	
	/**
	 * you can delete all products from basket
	 */
	public void clearBasket(){
		getBasket().removeAll(basket);
	}
	
	/**
	 * number of products in basket
	 * @return counter number of the Products in the basket
	 */
	public int numberOfProductsInBasket(){
		int counter = 0;
		for(@SuppressWarnings("unused") Product p:getBasket()){
			counter += 1;
		}
		return counter;
	}
	
	/**
	 * this method count all kg in the basket
	 * @return totalQuantity the quantity of the products in the basket
	 */
	public double totalGoodsKgInBasket(){
		double totalQuantity = 0;
		for (Product p:getBasket()) {
			totalQuantity += p.getProductConsumptionTotalKg();
		}
		setTotalGoodsKg(totalQuantity);
		return totalQuantity;
	}
	
	/**
	 * *this method total the net retail price of the materials
	 * @return total this is the net retail price sum of the products in the basket
	 */
	public double totalGoodsNetRetailPrice(){
		double total = 0;
		for (Product p:getBasket()) {
			total += p.getProductNetRetailSalePriceTotal();
		}
		setTotalGoodsNetPrice(total);
		return total;
	}
	
	/**
	 * this method total the gross retail price of the materials
	 * @return total this is the gross retail price sum of the products in the basket
	 */
	public double totalGoodsGrossRetailPrice(){
		double total = 0;
		for (Product p:getBasket()) {
			total += p.getProductGrossRetailSalePriceTotal();
		}
		setTotalGoodsGrossPrice(total);
		return total;
	}

	/**
	 * this method total the net wholesale price of the materials
	 * @return total this is the net wholesale price sum of the products in the basket
	 */
	public double totalGoodsNetWholesalePrice(){
		double total = 0;
		for (Product p:getBasket()) {
			total += p.getProductNetWholesaleSalePriceTotal();
		}
		setTotalGoodsNetPrice(total);
		return total;
	}
	
	/**
	 * this method total the gross wholesale price of the materials
	 * @return total this is the gross wholesale price sum of the products in the basket
	 */
	public double totalGoodsGrossWholesalePrice(){
		double total = 0;
		for (Product p:getBasket()) {
			total += p.getProductGrossWholesaleSalePriceTotal();
		}
		setTotalGoodsGrossPrice(total);
		return total;
	}
	
	/**
	 * you get retail or wholesale total price of the products from basket
	 * @param basket the basket from which you want to get the total retail/wholesale price of the products
	 */
	public void getRetailOrWholesaleTotalPriceProductsFromBasket(Basket basket){
		if (Flag.isRetailRadioButton()){
			basket.totalGoodsNetRetailPrice();
			basket.totalGoodsGrossRetailPrice();
		} else{
			basket.totalGoodsNetWholesalePrice();
			basket.totalGoodsGrossWholesalePrice();
		}
		basket.getTotalGoodsKg();
		basket.clearBasket();
	}
}
