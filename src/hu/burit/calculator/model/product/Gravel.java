package hu.burit.calculator.model.product;

/**
 * @author <b>Tamas Buri, 
 * tamasburi.jr@gmail.com, 
 * 0036/70-555-777-5</b>
 */
public class Gravel extends Product{
	//one bag of quartz is 25 kg
	private double gravelBag = 25;
	private double gravelBagConsumption;
	
	//getters and setters
	public double getGravelBag() {
		return gravelBag;
	}

	public void setGravelBag(double gravelBag) {
		this.gravelBag = gravelBag;
	}

	public double getGravelBagConsumption() {
		return gravelBagConsumption;
	}

	public void setGravelBagConsumption(double gravelBagConsumption) {
		this.gravelBagConsumption = gravelBagConsumption;
	}
	//end getters and setters
	
	/**
	 * constructor
	 * @param squareMeter
	 * @param productName
	 * @param productNetRetailSalePricePerKg
	 * @param productNetWholesaleSalePricePerKg
	 * @param productNetPurchasePriceEuro
	 * @param productConsumptionKgPerSquareMeter
	 */
	public Gravel(double squareMeter, String productName,
			double productNetRetailSalePricePerKg,
			double productNetWholesaleSalePricePerKg,
			double productNetPurchasePriceEuro,
			double productConsumptionKgPerSquareMeter) {
		super(squareMeter, productName, productNetRetailSalePricePerKg,
				productNetWholesaleSalePricePerKg, productNetPurchasePriceEuro,
				productConsumptionKgPerSquareMeter);
		
		setProduct(squareMeter);
	}

	/**
	 * this method calculate how many gravel bags of 25 kg do you need and set how many total kgs do you need
	 * @param squareMeter we set how many square meters do we need
	 * @return consumption because one gravel bag is 25 kg that's why we must get a number divisible by 25 
	 */
	@Override
	public double productConsumptionCalculateTotal(double squareMeter){
		double productConsumption = getProductConsumptionKgPerSquareMeter()*squareMeter;
		int consumption = (int) (productConsumption/getGravelBag());
		if (productConsumption%getGravelBag()!=0) {
			consumption+=1;
			setProductConsumptionTotalKg(consumption * gravelBag);
			return consumption;
		} else{
			setProductConsumptionTotalKg(consumption * gravelBag);
			return consumption;
		}
	};
}
