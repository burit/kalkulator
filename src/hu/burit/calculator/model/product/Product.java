package hu.burit.calculator.model.product;

import hu.burit.calculator.model.dao.VATDAO;
import hu.burit.calculator.model.databaseconnection.DatabaseConnection;
import hu.burit.calculator.model.utils.EuroExchangeRate;
import hu.burit.calculator.model.utils.VAT;

import java.util.List;


/**
 * @author <b>Tamas Buri, 
 * tamasburi.jr@gmail.com, 
 * 0036/70-555-777-5</b>
 */
public class Product {

	private String productName;
	private double productNetRetailSalePricePerKg;
	private double productNetRetailSalePriceTotal;

	private double productGrossRetailSalePricePerKg;
	private double productGrossRetailSalePriceTotal;

	private double productNetWholesaleSalePricePerKg;
	private double productNetWholesaleSalePriceTotal;

	private double productGrossWholesaleSalePricePerKg;
	private double productGrossWholesaleSalePriceTotal;

	private double productNetCostPriceEuro;
	private double productGrossCostPriceEuro;

	private double productNetCostTotalPriceEuro;
	private double productGrossCostTotalPriceEuro;

	private double productConsumptionKgPerSquareMeter;
	private double productConsumptionTotalKg;
	private double squareMeter;

	private double profit;
	private int id;
	private EuroExchangeRate euroExchangeRate;
	private VATDAO vATDAO;
	private VAT vat;

	
	/**
	 *  constructor
	 * @param squareMeter
	 * @param productName
	 * @param productNetRetailSalePricePerKg
	 * @param productNetWholesaleSalePricePerKg
	 * @param productNetCostPriceEuro
	 * @param productConsumptionKgPerSquareMeter
	 */
	public Product(
		double squareMeter, 
		String productName, 
		double productNetRetailSalePricePerKg,
		double productNetWholesaleSalePricePerKg,
		double productNetCostPriceEuro,
		double productConsumptionKgPerSquareMeter) {
		super();
		
		this.productName = productName;
		this.productNetRetailSalePricePerKg = productNetRetailSalePricePerKg;
		this.productNetWholesaleSalePricePerKg = productNetWholesaleSalePricePerKg;
		this.productNetCostPriceEuro = productNetCostPriceEuro;
		this.productConsumptionKgPerSquareMeter = productConsumptionKgPerSquareMeter;
		this.squareMeter = squareMeter;
		
		
		//we set all the value of the product 
		setProduct(squareMeter);
	}
	
	// getters and setters
	public double getProductNetCostTotalPriceEuro() {
		return productNetCostTotalPriceEuro;
	}

	public void setProductNetCostTotalPriceEuro(
			double productNetCostTotalPriceEuro) {
		this.productNetCostTotalPriceEuro = productNetCostTotalPriceEuro;
	}

	public double getProductGrossCostTotalPriceEuro() {
		return productGrossCostTotalPriceEuro;
	}

	public void setProductGrossCostTotalPriceEuro(
			double productGrossCostTotalPriceEuro) {
		this.productGrossCostTotalPriceEuro = productGrossCostTotalPriceEuro;
	}

	public void setProductGrossCostPriceEuro(
			double productGrossCostPriceEuro) {
		this.productGrossCostPriceEuro = productGrossCostPriceEuro;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public double getProductNetRetailSalePricePerKg() {
		return productNetRetailSalePricePerKg;
	}

	public void setProductNetRetailSalePricePerKg(
			double productNetRetailSalePricePerKg) {
		this.productNetRetailSalePricePerKg = productNetRetailSalePricePerKg;
	}

	public double getProductNetRetailSalePriceTotal() {
		return productNetRetailSalePriceTotal;
	}

	public void setProductNetRetailSalePriceTotal(double productNetRetailSalePriceTotal) {
		this.productNetRetailSalePriceTotal = productNetRetailSalePriceTotal;
	}

	public double getProductGrossRetailSalePricePerKg() {
		return productGrossRetailSalePricePerKg;
	}

	public void setProductGrossRetailSalePricePerKg() {
		this.productGrossRetailSalePricePerKg = (this.productNetRetailSalePricePerKg * vat.getVat());
	}

	public double getProductGrossRetailSalePriceTotal() {
		return productGrossRetailSalePriceTotal;
	}

	public void setProductGrossRetailSalePriceTotal(
			double productGrossRetailSalePriceTotal) {
		this.productGrossRetailSalePriceTotal = productGrossRetailSalePriceTotal;
	}

	public double getProductNetWholesaleSalePricePerKg() {
		return productNetWholesaleSalePricePerKg;
	}

	public void setProductNetWholesaleSalePricePerKg(
			double productNetWholesaleSalePricePerKg) {
		this.productNetWholesaleSalePricePerKg = productNetWholesaleSalePricePerKg;
	}

	public double getProductNetWholesaleSalePriceTotal() {
		return productNetWholesaleSalePriceTotal;
	}

	public void setProductNetWholesaleSalePriceTotal(
			double productNetWholesaleSalePriceTotal) {
		this.productNetWholesaleSalePriceTotal = productNetWholesaleSalePriceTotal;
	}

	public double getProductGrossWholesaleSalePricePerKg() {
		return productGrossWholesaleSalePricePerKg;
	}

	public void setProductGrossWholesaleSalePricePerKg() {
		this.productGrossWholesaleSalePricePerKg = this.productNetWholesaleSalePricePerKg * vat.getVat();
	}

	public double getProductGrossWholesaleSalePriceTotal() {
		return productGrossWholesaleSalePriceTotal;
	}

	public void setProductGrossWholesaleSalePriceTotal(
			double productGrossWholesaleSalePriceTotal) {
		this.productGrossWholesaleSalePriceTotal = productGrossWholesaleSalePriceTotal;
	}

	public double getProductNetCostPriceEuro() {
		return productNetCostPriceEuro;
	}

	public void setProductNetCostPriceEuro(
			double productNetCostPriceEuro) {
		this.productNetCostPriceEuro = productNetCostPriceEuro;
	}

	public double getProductGrossCostPriceEuro() {
		return productGrossCostPriceEuro;
	}

	public void setProductGrossCostPriceEuro() {
		this.productGrossCostPriceEuro = this.productNetCostPriceEuro * vat.getVat();
	}

	public double getProductConsumptionKgPerSquareMeter() {
		return productConsumptionKgPerSquareMeter;
	}

	public void setProductConsumptionKgPerSquareMeter(double productConsumptionKgPerSquareMeter) {
		this.productConsumptionKgPerSquareMeter = productConsumptionKgPerSquareMeter;
	}

	public double getProductConsumptionTotalKg() {
		return productConsumptionTotalKg;
	}

	public void setProductConsumptionTotalKg(double productConsumptionTotalKg) {
		this.productConsumptionTotalKg = productConsumptionTotalKg;
	}

	public double getSquareMeter() {
		return squareMeter;
	}

	public void setSquareMeter(double squareMeter) {
		this.squareMeter = squareMeter;
	}

	public double getEuroExchangeRate() {
		return euroExchangeRate.getEuroExchangeRate();
	}

	public void setEuroExchangeRate(EuroExchangeRate euroExchangeRate) {
		this.euroExchangeRate = euroExchangeRate;
	}

	public double getProfit() {
		return profit;
	}

	public void setProfit(double profit) {
		this.profit = profit;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	// end getters and setters
	
	
	/**
	 * we set all values of the product
	 * @param squareMeter you can add how many square meters do you need
	 */
	protected void setProduct(double squareMeter){
		initVAT();
		euroExchangeRate = new EuroExchangeRate();
		productConsumptionCalculateTotal(squareMeter);
		productNetRetailSalePriceCalculateTotal();
		setProductGrossRetailSalePricePerKg();
		setProductGrossWholesaleSalePricePerKg();
		setProductGrossCostPriceEuro();
		productGrossRetailSalePriceCalculateTotal();
		productNetWholesaleSalePriceCalculateTotal();
		productGrossWholesaleSalePriceCalculateTotal();
		productNetCostPriceCalculateTotal();
		productGrossCostPriceCalculateTotal();
	}
	
	
	/**
	 * we initialize VAT object
	 */
	public void initVAT(){
		//implements VatDAO
		vATDAO = new VATDAO(DatabaseConnection.getInstance().getConn());
		final List<VAT> listVat = vATDAO.findAll();
		
		if(listVat != null ){
			if(!listVat.isEmpty()){
				//init Vat
				if(vat == null){
					vat = new VAT(listVat.get(0).getVat());
				}
			}
		}
	}
	
	/**
	 *  we get how many kg material do we need for given square meter
	 * @param squareMeter you can add how many square meters do you need
	 * @return getProductConsumptionTotalKg() we get how many total kgs of material do we need
	 */
	public double productConsumptionCalculateTotal(double squareMeter) {
		setProductConsumptionTotalKg(getProductConsumptionKgPerSquareMeter() * squareMeter);
		return getProductConsumptionTotalKg();
	}

	/**
	 * we calculate the total retail net sale price  
	 * @return getProductNetRetailSalePriceTotal() we get how much is the total retail net sale price
	 */
	public double productNetRetailSalePriceCalculateTotal() {
		setProductNetRetailSalePriceTotal(getProductNetRetailSalePricePerKg()* getProductConsumptionTotalKg());
		return getProductNetRetailSalePriceTotal();
	}

	/**
	 * we calculate the total retail gross sale price 
	 * @return getProductGrossRetailSalePriceTotal() we get how much is the total retail gross sale price
	 */
	public double productGrossRetailSalePriceCalculateTotal() {
		setProductGrossRetailSalePriceTotal(productNetRetailSalePriceCalculateTotal() * vat.getVat());
		return getProductGrossRetailSalePriceTotal();
	}

	/**
	 * we calculate the total wholesale net sale price 
	 * @return getProductNetWholesaleSalePriceTotal() we get how much is the total wholesale net sale price
	 */
	public double productNetWholesaleSalePriceCalculateTotal() {
		setProductNetWholesaleSalePriceTotal(getProductNetWholesaleSalePricePerKg() * getProductConsumptionTotalKg());
		return getProductNetWholesaleSalePriceTotal();
	}

	/**
	 * we calculate the total wholesale gross sale price
	 * @return getProductGrossWholesaleSalePriceTotal() we get how much is the total wholesale gross sale price
	 */
	public double productGrossWholesaleSalePriceCalculateTotal() {
		setProductGrossWholesaleSalePriceTotal(productNetWholesaleSalePriceCalculateTotal() * vat.getVat());
		return getProductGrossWholesaleSalePriceTotal();
	}

	/**
	 * we calculate the total net cost price 
	 * @return getProductNetCostTotalPriceEuro() we get how much is the total net cost price
	 */
	public double productNetCostPriceCalculateTotal() {
		setProductNetCostTotalPriceEuro(getEuroExchangeRate() * getProductNetCostPriceEuro() * getProductConsumptionTotalKg());
		return getProductNetCostTotalPriceEuro();
	}

	/**
	 * we calculate the total gross cost price 
	 * @return getProductGrossCostTotalPriceEuro() we get how much is the total gross cost price
	 */
	public double productGrossCostPriceCalculateTotal() {
		setProductGrossCostTotalPriceEuro(productNetCostPriceCalculateTotal() * vat.getVat());
		return getProductGrossCostTotalPriceEuro();
	}
}
