package hu.burit.calculator.model.databaseconnection;

import java.sql.*;

/**
 * @author <b>Tamas Buri, 
 * tamasburi.jr@gmail.com, 
 * 0036/70-555-777-5</b>
 * 
 * connect to the "calculator" database and use Singleton Design Pattern (Bill Pugh solution)
 */
public class DatabaseConnection {
	
	private Connection conn ;
	private String databaseName = "calculator";
	private String userName = "root";
	private String password = "";
	
	//getter
	public Connection getConn() {
		return conn;
	}
	//end getter
	
	/**
	 * private constructor
	 */
	private DatabaseConnection(){
		if(conn == null){
			try {
				conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"+databaseName,userName,password);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * this class creates one instance from DatabaseConnection class
	 */
	private static class SingletonHolder{
		private final static DatabaseConnection INSTANCE = new DatabaseConnection();
	}

	/**
	 * we get one instance from DatabaseConnection class
	 * @return SingletonHolder.INSTANCE
	 */
	public static DatabaseConnection getInstance(){
		return SingletonHolder.INSTANCE;
	}
}

