package hu.burit.calculator.model.dao;

import java.util.List;

/**
 * @author <b>Tamas Buri, 
 * tamasburi.jr@gmail.com, 
 * 0036/70-555-777-5</b>
 */
public interface BaseDAO<O> {

	/**
	 * insert this object
	 * @param obj
	 */
	public int insert(O obj);
	
	/**
	 * delete this object
	 * @param obj
	 */
	public int delete(O obj);
	
	/**
	 * update this object
	 * @param obj
	 */
	public int update(O obj);
	
	/**
	 * you can find one thing by it's name
	 * @param productName
	 */
	public List<O> find(String productName);
	
	/**
	 * you can find all those things you want
	 */
	public List<O> findAll();
}
