package hu.burit.calculator.model.dao;

import hu.burit.calculator.model.utils.WholesaleDiscount;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.sql.PreparedStatement;

/**
 * @author <b>Tamas Buri, 
 * tamasburi.jr@gmail.com, 
 * 0036/70-555-777-5</b>
 */
public class WholesaleDiscountDAO implements BaseDAO<WholesaleDiscount>{

	private static final String INSERTSQL = "INSERT INTO wholesale_discount(wholesale_discount_percent) VALUES (?)";
	private static final String SELECTSQL = "SELECT wholesale_discount_id,wholesale_discount_percent FROM wholesale_discount";
	private static final String UPDATESQL = "UPDATE wholesale_discount SET wholesale_discount_percent=? where wholesale_discount_id=?";
	
	private PreparedStatement preparedStatement;
	private Connection connection;
	private WholesaleDiscount wholesaleDiscount;
	
	
	public WholesaleDiscountDAO(Connection conn){
		this.connection = conn;
	}
	
	//getters and setters
	public PreparedStatement getPst() {
		return preparedStatement;
	}
	
	public Connection getConnection() {
		return connection;
	}
	//end getters and setters
	
	/**
	 * insert this WholesaleDiscount object into the database
	 * @param obj you can put into database only WholesaleDiscount objects
	 * @return count number of how many WholesaleDiscount objects could you put into the database, if none you get back -1
	 */
	@Override
	public int insert(WholesaleDiscount obj) {
		try {
			preparedStatement = getConnection().prepareStatement(INSERTSQL);
			preparedStatement.setDouble(1,obj.getWholesaleDiscountPercent());
			int count = preparedStatement.executeUpdate();
			return count;
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}
	}

	
	/**
	 * this method is not yet doing anything
	 * @param obj you can delete from the database only WholesaleDiscount objects
	 */
	@Override
	public int delete(WholesaleDiscount obj) {
		return 0;
	}

	
	/**
	 * update this WholesaleDiscount object in the database
	 * @param obj you can update this WholesaleDiscount object in the database
	 * @return count number of how many WholesaleDiscount objects could you update in the database, if none you get back -1
	 */
	@Override
	public int update(WholesaleDiscount obj) {
		try {
			preparedStatement = getConnection().prepareStatement(UPDATESQL);
			preparedStatement.setDouble(1, obj.getWholesaleDiscountPercent());
			preparedStatement.setInt(2, 1);
			int count = preparedStatement.executeUpdate();
			return count;
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}
	}

	
	/**
	 * you can get all the WholesaleDiscount objects from database
	 * @return wholesaleDiscountList we get back a list of WholesaleDiscount objects from database
	 */
	@Override
	public List<WholesaleDiscount> findAll(){
		List<WholesaleDiscount> wholesaleDiscountList = new ArrayList<>();
		
		try {
			preparedStatement = getConnection().prepareStatement(SELECTSQL);
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				double wholesaleDiscountNumber = (resultSet.getDouble(2)-1)*100;
				wholesaleDiscount = new WholesaleDiscount(wholesaleDiscountNumber);
				wholesaleDiscountList.add(wholesaleDiscount);
			}
			return wholesaleDiscountList;
		} catch (SQLException e) {
			e.printStackTrace();
			return wholesaleDiscountList;
		}
	}

	
	/**
	 * this method is not yet doing anything!! Later you can find item by name and put in a list
	 * @param productName
	 * @return wholesaleDiscountList
	 */
	@Override
	public List<WholesaleDiscount> find(String productName) {
		List<WholesaleDiscount> wholesaleDiscountList = new ArrayList<>();
		return wholesaleDiscountList;
	}

}
