package hu.burit.calculator.model.dao;

import hu.burit.calculator.model.product.Product;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.sql.PreparedStatement;


/**
 * @author <b>Tamas Buri, 
 * tamasburi.jr@gmail.com, 
 * 0036/70-555-777-5</b>
 */
public class ProductDAO implements BaseDAO<Product>{

	private static final String INSERTSQL = "INSERT INTO product(product_name, product_net_retail_sale_price, product_purchase_price_euro, product_consumption_kg_per_squaremeter) values (?,?,?,?)";
	private static final String DELETESQL = "DELETE FROM product WHERE product_id=?";
	private static final String UPDATESQL = "UPDATE product set product_name=?, product_net_retail_sale_price=?, product_purchase_price_euro=?, product_consumption_kg_per_squaremeter=? WHERE product_id=?";
	private static final String SELECTSQL = "SELECT product_name, product_net_retail_sale_price, product_purchase_price_euro, product_consumption_kg_per_squaremeter FROM product";
	private static final String SELECTPRODUCTNAMESQL = "SELECT product_id, product_name, product_net_retail_sale_price, product_purchase_price_euro, product_consumption_kg_per_squaremeter FROM product WHERE product_name=?";
	
	private PreparedStatement pst;
	private Connection conn;
	
	/**
	 * constructor with one parameter
	 * @param conn
	 */
	public ProductDAO(Connection conn){
		this.conn = conn;
	}
	
	//getters and setters	
	public Connection getConn() {
		return conn;
	}

	public PreparedStatement getPst() {
		return pst;
	}
	//end getters and setters
	
	/**
	 * insert this Product object into the database
	 * @param obj you can put into database only Product objects
	 * @return count number of how many Product objects could you put into the database, if none you get back -1
	 */
	@Override
	public int insert(Product obj) {
		try {
			pst = getConn().prepareStatement(INSERTSQL);
			pst.setString(1, obj.getProductName());
			pst.setString(2, String.valueOf(obj.getProductNetRetailSalePricePerKg()));
			pst.setString(3, String.valueOf(obj.getProductNetCostPriceEuro()));
			pst.setString(4, String.valueOf(obj.getProductConsumptionKgPerSquareMeter()));
			int count = pst.executeUpdate();
            return count;
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}
	}


	/**
	 * delete this Product object from database
	 * @param obj you can delete this Product object from database
	 * @return count number of how many Product objects could you delete from database, if none you get back -1
	 */
	@Override
	public int delete(Product obj) {
		try{
			pst = getConn().prepareStatement(DELETESQL);
			pst.setInt(1, obj.getId());
			int count = pst.executeUpdate();
			return count;
		}catch(SQLException e){
			e.printStackTrace();
			return -1;
		}
	}

	/**
	 * update this Product object in the database
	 * @param obj you can update this Product object in the database
	 * @return count number of how many Product objects could you update in the database, if none you get back -1
	 */
	@Override
	public int update(Product obj) {
		try {
			pst = getConn().prepareStatement(UPDATESQL);
			pst.setString(1, obj.getProductName());
			pst.setString(2, String.valueOf(obj.getProductNetRetailSalePricePerKg()));
			pst.setString(3, String.valueOf(obj.getProductNetCostPriceEuro()));
			pst.setString(4, String.valueOf(obj.getProductConsumptionKgPerSquareMeter()));
			pst.setInt(5, obj.getId());
			int count = pst.executeUpdate();
            return count;
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}
	}

	/**
	 * you can get all the Product objects from database
	 * @return productList we get back a list of Product objects from database
	 */
	@Override
	public List<Product> findAll() {
		List<Product> productList = new ArrayList<>();
		try{
			pst = getConn().prepareStatement(SELECTSQL);
			ResultSet rs = pst.executeQuery();
			while(rs.next()){
				Product productsObject = new Product(0,rs.getString(1),rs.getDouble(2), rs.getDouble(3), 0,rs.getDouble(4));
				productList.add(productsObject);
			}
			return productList;
		}catch(SQLException e){
			e.printStackTrace();
			return productList;
		}
	}
	
	/**
	 * you can find item by name and put in an array. It can contains only one element
	 * @param productName name of the Product object you want to find
	 * @return array we get back an array of one Product object
	 */
	public Product[] findArray(String productName){
		Product[] array = new Product[1];
		try{
			pst = getConn().prepareStatement(SELECTPRODUCTNAMESQL);
			pst.setString(1, productName);
			ResultSet rs = pst.executeQuery();
			while(rs.next()){
				Product productsObject = new Product(0,rs.getString(2),rs.getDouble(3), rs.getDouble(4), 0,rs.getDouble(5));
				productsObject.setId(rs.getInt(1));
				array[0] = productsObject;
			}
			return array;
		}catch(SQLException e){
			e.printStackTrace();
			return array;
		}
	}
	
	/**
	 * you can find item by name and put in a list
	 * @param productName name of the Product object you want to find
	 * @return Product we get back a list of Product objects
	 */
	@Override
	public List<Product> find(String productName) {
		
		List<Product> productList = new ArrayList<>();
		try{
			pst = getConn().prepareStatement(SELECTPRODUCTNAMESQL);
			pst.setString(1, productName);
			ResultSet rs = pst.executeQuery();
			while(rs.next()){
				Product productsObject = new Product(0,rs.getString(2),rs.getDouble(3), rs.getDouble(4), 0,rs.getDouble(5));
				productsObject.setId(rs.getInt(1));
				productList.add(productsObject);
			}
			return productList;
		}catch(SQLException e){
			e.printStackTrace();
			return productList;
		}
	}
}
