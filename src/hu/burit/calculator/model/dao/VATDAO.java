package hu.burit.calculator.model.dao;

import hu.burit.calculator.model.utils.VAT;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.sql.PreparedStatement;

/**
 * @author <b>Tamas Buri, 
 * tamasburi.jr@gmail.com, 
 * 0036/70-555-777-5</b>
 */
public class VATDAO implements BaseDAO<VAT>{

	private static final String INSERTSQL = "INSERT INTO vat(vat_percent) VALUES (?)";
	private static final String SELECTSQL = "SELECT vat_id,vat_percent FROM vat";
	private static final String UPDATESQL = "UPDATE vat SET vat_percent=? where vat_id=?";
	
	private PreparedStatement preparedStatement;
	private Connection connection;
	private VAT vat;
	
	/**
	 * constructor with one parameter
	 * @param conn
	 */
	public VATDAO(Connection conn){
		this.connection = conn;
	}
	
	//getters and setters
	public PreparedStatement getPreparedStatement() {
		return preparedStatement;
	}

	public Connection getConnection() {
		return connection;
	}
	//end getters and setters
	
	/**
	 * insert this Vat object into the database
	 * @param obj you can put into database only VAT objects
	 * @return count number of how many VAT objects could you put into the database, if none you get back -1
	 */
	@Override
	public int insert(VAT obj) {
		try {
			preparedStatement = getConnection().prepareStatement(INSERTSQL);
			preparedStatement.setDouble(1, obj.getVat());
			int count = preparedStatement.executeUpdate();
			return count;
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}
	}

	
	/**
	 * this method is not yet doing anything
	 * @param obj you can delete from the database only VAT objects
	 * @return 0
	 */
	@Override
	public int delete(VAT obj) {
		return 0;
	}


	/**
	 * update this VAT object in the database
	 * @param obj you can update this VAT object in the database
	 * @return count number of how many VAT objects could you update in the database, if none you get back -1
	 */
	@Override
	public int update(VAT obj) {
		try {
			preparedStatement = getConnection().prepareStatement(UPDATESQL);
			preparedStatement.setDouble(1, obj.getVat());
			preparedStatement.setInt(2, obj.getVatId());
			int count = preparedStatement.executeUpdate();
			return count;
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}
	}

	
	/**
	 * you can get all the VAT objects from database
	 * @return VATList we get back a list of VAT objects from database
	 */
	@Override
	public List<VAT> findAll() {
		List<VAT> VATList = new ArrayList<>();
		
		try {
			preparedStatement = getConnection().prepareStatement(SELECTSQL);
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				double vatNumber = (resultSet.getDouble(2)-1)*100;
				vat = new VAT(vatNumber,resultSet.getInt(1));
				vat.setVatId(resultSet.getInt(1));
				VATList.add(vat);
			}
			return VATList;
		} catch (SQLException e) {
			e.printStackTrace();
			return VATList;
		}
	}

	
	/**
	 * this method is not yet doing anything!! Later you can find item by name and put in a list
	 * @param productName
	 * @return VATList
	 */
	@Override
	public List<VAT> find(String productName) {
		List<VAT> VATList = new ArrayList<>();
		return VATList;
	}
}
