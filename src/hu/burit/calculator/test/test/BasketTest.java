package hu.burit.calculator.test.test;

import hu.burit.calculator.model.basket.Basket;
import hu.burit.calculator.model.product.Product;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author <b>Tamas Buri, 
 * tamasburi.jr@gmail.com, 
 * 0036/70-555-777-5</b>
 */
public class BasketTest {
	private Basket basket;
	private Product product1;
	private Product product2;
	
	
	/**
	 * we create instance of Product class we can use for test
	 */
	@Before
	public void init(){
		basket = new Basket();
		product1 = new Product(2, "VA01", 1.2, 1, 1, 10);
		product2 = new Product(4, "SIL04", 2.4, 2, 2, 20);
	}
	
	/**
	 * we add Product objects to basket than we check it happened or not
	 */
	@Test
	public void testnumberOfProductsInBasket(){
		basket.addProductsToBasket(product1);
		basket.addProductsToBasket(product2);
		assertEquals(2, basket.numberOfProductsInBasket(), 0 );
	}
	
	/**
	 *  we add Product objects to basket than we check it happened or not
	 *  we clear the basket than check it is empty or not
	 */
	@Test
	public void testClearBasket(){
		basket.addProductsToBasket(product1);
		basket.addProductsToBasket(product2);
		assertEquals(2, basket.numberOfProductsInBasket(), 0 );
		basket.clearBasket();
		assertEquals(0, basket.numberOfProductsInBasket(), 0 );
	}
	
	/**
	 *  we add Product objects to basket than we check it happened or not
	 *  we delete one product from the basket than check it happened or not
	 */
	@Test
	public void testDeleteProductsFromBasket(){
		basket.addProductsToBasket(product1);
		basket.addProductsToBasket(product2);
		assertEquals(2, basket.numberOfProductsInBasket(), 0 );
		basket.deleteProductsFromBasket(1);
		assertEquals(1, basket.numberOfProductsInBasket(), 0 );
	}
	
	/**
	 * we add Product objects to basket than we check those objects total net wholesale price
	 */
	@Test
	public void testTotalGoodsNetWholesalePrice(){
		basket.addProductsToBasket(product1);
		basket.addProductsToBasket(product2);
		assertEquals(180, basket.totalGoodsNetWholesalePrice(), 0 );
	}
	
	/**
	 * we add Product objects to basket than we check those objects total gross wholesale price
	 */
	@Test
	public void testTotalGoodsGrossWholesalePrice(){
		basket.addProductsToBasket(product1);
		basket.addProductsToBasket(product2);
		assertEquals(228.6, basket.totalGoodsGrossWholesalePrice(), 0 );
	}
	
	/**
	 * we add Product objects to basket than we check those objects total net retail price
	 */
	@Test
	public void testTotalGoodsNetRetailPrice(){
		basket.addProductsToBasket(product1);
		basket.addProductsToBasket(product2);
		assertEquals(216, basket.totalGoodsNetRetailPrice(), 0 );
	}
	
	/**
	 * we add Product objects to basket than we check those objects total gross retail price
	 */
	@Test
	public void testTotalGoodsGrossRetailPrice(){
		basket.addProductsToBasket(product1);
		basket.addProductsToBasket(product2);
		assertEquals(274.32, basket.totalGoodsGrossRetailPrice(), 0 );
	}
	
	/**
	 * we add Product objects to basket than we check those objects total weight in kg
	 */
	@Test
	public void testTotalGoodsKgInBasket(){
		basket.addProductsToBasket(product1);
		basket.addProductsToBasket(product2);
		assertEquals(100, basket.totalGoodsKgInBasket(), 0 );
	}
}
