package hu.burit.calculator.test.test;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

/**
 * @author <b>Tamas Buri, 
 * tamasburi.jr@gmail.com, 
 * 0036/70-555-777-5</b>
 */
public class TestRunner{

	/**
	 * Launch tests
	 */
	public static void main(String[] args) {
      Result result = JUnitCore.runClasses(
    		  VATTest.class,
    		  WholesaleDiscountTest.class,
    		  ProductTest.class,
    		  BasketTest.class,
    		  GravelTest.class);
      for (Failure failure : result.getFailures()) {
         System.out.println(failure.toString());
      }
      System.out.println(result.wasSuccessful());
	}
}
