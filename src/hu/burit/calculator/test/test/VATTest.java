package hu.burit.calculator.test.test;

import hu.burit.calculator.model.dao.VATDAO;
import hu.burit.calculator.model.databaseconnection.DatabaseConnection;
import hu.burit.calculator.model.utils.VAT;

import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;


/**
 * @author <b>Tamas Buri, 
 * tamasburi.jr@gmail.com, 
 * 0036/70-555-777-5</b>
 */
public class VATTest{
	private VAT vat;
	private VATDAO vATDAO;
	
	/**
	 * we create instance of VAT class we can use for test
	 */
	@Before
	public void init(){
		vATDAO = new VATDAO(DatabaseConnection.getInstance().getConn());
		final List<VAT> listVat = vATDAO.findAll();
		
		//init Vat
		if(vat == null){
			vat = new VAT(listVat.get(0).getVat());
		}
	}
	
	/**
	 * we check the vat procent
	 */
	@Test
	public void VATTest1(){
		assertEquals(1.27, vat.getVat(), 0);
	}	
}
