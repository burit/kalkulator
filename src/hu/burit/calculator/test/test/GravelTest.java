package hu.burit.calculator.test.test;

import hu.burit.calculator.model.product.Gravel;

import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


/**
 * @author <b>Tamas Buri, 
 * tamasburi.jr@gmail.com, 
 * 0036/70-555-777-5</b>
 */
public class GravelTest {
	private static Gravel gravel;
	
	/**
	 * we create instance of Gravel class we can use for test
	 */
	@BeforeClass
	public static void init(){
		gravel = new Gravel(2, "S", 1.2, 1, 1, 10);
	}
	
	/**
	 * we check how many kgs of Gravel object do we need total
	 */
	@Test
	public void testProductConsumptionCalculateTotal(){
		assertEquals(1, gravel.productConsumptionCalculateTotal(1), 0);
		assertEquals(1, gravel.productConsumptionCalculateTotal(2), 0);
		assertEquals(2, gravel.productConsumptionCalculateTotal(3), 0);
	}
}
