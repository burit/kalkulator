package hu.burit.calculator.test.test;

import hu.burit.calculator.model.product.Product;

import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author <b>Tamas Buri, 
 * tamasburi.jr@gmail.com, 
 * 0036/70-555-777-5</b>
 */
public class ProductTest {
	private static Product product;
	
	/**
	 * we create instance of Product class we can use for test
	 */
	@BeforeClass
	public static void init(){
		product = new Product(2, "VA01", 1.2, 1, 1, 10);
		product.getProductName();
	}
	
	/**
	 * we check how many kgs of Product object do we need total
	 */
	@Test
	public void testProductConsumptionTotalKg(){
		assertEquals(20, product.getProductConsumptionTotalKg(), 0);
	}
	
	/**
	 * we check how many kgs per square meter of Product object do we need 
	 */
	@Test
	public void testProductConsumptionKgPerSquareMeter(){
		assertEquals(10, product.getProductConsumptionKgPerSquareMeter(), 0);
	}
	
	/**
	 * we check the name of the Product object
	 */
	@Test
	public void testProductName(){
		assertEquals("VA01", product.getProductName());
	}
	
	/**
	 * we check the total gross retail sale price of the Product object
	 */
	@Test
	public void testProductGrossRetailSalePriceTotal(){
		assertEquals(30.48, product.getProductGrossRetailSalePriceTotal(), 0);
	}
	
	/**
	 * we check the gross retail sale price per kg of the Product object 
	 */
	@Test
	public void testProductGrossRetailSalePricePerKg(){
		assertEquals(1.524, product.getProductGrossRetailSalePricePerKg(), 0);
	}
	
	/**
	 * we check the net retail sale price per kg of the Product object
	 */
	@Test
	public void testProductNetRetailSalePricePerKg(){
		assertEquals(1.2, product.getProductNetRetailSalePricePerKg(), 0);
	}
	
	/**
	 * we check the total net retail sale price of the Product object
	 */
	@Test
	public void testProductNetRetailSalePriceTotal(){
		assertEquals(24, product.getProductNetRetailSalePriceTotal(), 0);
	}
	
	/**
	 * we check the gross wholesale sale price per kg of the Product object
	 */
	@Test
	public void testProductGrossWholesaleSalePricePerKg(){
		assertEquals(1.27, product.getProductGrossWholesaleSalePricePerKg(), 0);
	}
	
	/**
	 * we check the net wholesale sale price per kg of the Product object
	 */
	@Test
	public void testProductNetWholesaleSalePricePerKg(){
		assertEquals(1, product.getProductNetWholesaleSalePricePerKg(), 0);
	}
	
	/**
	 * we check the total net wholesale sale price of the Product object
	 */
	@Test
	public void testProductNetWholesaleSalePriceToTal(){
		assertEquals(20, product.getProductNetWholesaleSalePriceTotal(), 0);
	}
	
	/**
	 * we check the total gross wholesale sale price of the Product object
	 */
	@Test
	public void testProductGrossWholesaleSalePriceToTal(){
		assertEquals(25.4, product.getProductGrossWholesaleSalePriceTotal(), 0);
	}
	
	/**
	 * we check the net cost price in euro per kg of the Product object
	 */
	@Test
	public void testProductNetCostPriceEuroPerKg(){
		assertEquals(1, product.getProductNetCostPriceEuro(), 0);
	}
	
	/**
	 * we check the net gross cost price in euro per kg of the Product object
	 */
	@Test
	public void testProductGrossCostPriceEuroPerKg(){
		assertEquals(1.27, product.getProductGrossCostPriceEuro(), 0);
	}
}
