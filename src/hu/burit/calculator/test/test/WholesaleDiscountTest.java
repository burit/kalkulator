package hu.burit.calculator.test.test;

import hu.burit.calculator.model.dao.WholesaleDiscountDAO;
import hu.burit.calculator.model.databaseconnection.DatabaseConnection;
import hu.burit.calculator.model.utils.WholesaleDiscount;

import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author <b>Tamas Buri, 
 * tamasburi.jr@gmail.com, 
 * 0036/70-555-777-5</b>
 */
public class WholesaleDiscountTest{
	private double wholesaleDiscountPercent;
	private WholesaleDiscountDAO wholesaleDiscountDAO;
	
	@Test
	public void wholesaleDiscountTest1(){
		wholesaleDiscountDAO = new WholesaleDiscountDAO(DatabaseConnection.getInstance().getConn());
		List<WholesaleDiscount> list = wholesaleDiscountDAO.findAll();
		
		wholesaleDiscountPercent = list.get(0).getWholesaleDiscountPercent();
		
		assertEquals(1.2, wholesaleDiscountPercent, 0);
	}
}
