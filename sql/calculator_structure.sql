-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jul 05, 2013 at 01:40 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `calculator`
--
CREATE DATABASE `calculator`;
-- --------------------------------------------------------

--
-- Table structure: `product`
--
CREATE TABLE IF NOT EXISTS `product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(45) NOT NULL,
  `product_net_retail_sale_price` double NOT NULL,
  `product_purchase_price_euro` double NOT NULL,
  `product_consumption_kg_per_squaremeter` double NOT NULL,
  PRIMARY KEY (`product_id`),
  UNIQUE KEY `product_name_UNIQUE` (`product_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Table structure: `vat`
--
CREATE TABLE IF NOT EXISTS `vat` (
  `vat_id` int(11) NOT NULL AUTO_INCREMENT,
  `vat_percent` double NOT NULL,
  PRIMARY KEY (`vat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Table structure: `wholesale_discount`
--
CREATE TABLE IF NOT EXISTS `wholesale_discount` (
  `wholesale_discount_id` int(11) NOT NULL AUTO_INCREMENT,
  `wholesale_discount_percent` double NOT NULL,
  PRIMARY KEY (`wholesale_discount_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;



/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
