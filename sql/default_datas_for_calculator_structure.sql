-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jul 05, 2013 at 01:40 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `calculator`
--

-- --------------------------------------------------------


--
-- Default data for table `product`
--

INSERT INTO `product` (`product_id`, `product_name`, `product_net_retail_sale_price`, `product_purchase_price_euro`, `product_consumption_kg_per_squaremeter`) VALUES
(1, 'Eaa/a', 100, 7, 0.89),
(2, 'Eaa/b', 100, 7, 0.45),
(3, 'S gravel horizontal', 100, 7, 13),
(4, 'SC gravel horizontal', 100, 7, 13),
(5, 'SD gravel horizontal', 100, 7, 13),
(6, 'SM gravel horizontal', 100, 7, 13),
(7, 'SF gravel horizontal', 100, 7, 13),
(8, 'Sad22 horizontal', 100, 7, 0.7),
(9, 'Sil04', 100, 7, 2.5),
(10, 'Va01', 100, 7, 0.3),
(11, 'S gravel vertical', 100, 7, 10),
(12, 'SC gravel vertical', 10,  7, 10),
(13, 'SD gravel vertical', 100,  7, 10),
(14, 'SM gravel vertical', 100,  7, 10),
(15, 'SF gravel vertical', 100,  7, 10),
(16, 'Sad22 vertical', 100, 7, 0.7)


--
-- Dumping data for table `vat`
--
INSERT INTO `vat` (`vat_id`, `vat_percent`) VALUES
(1, 1.27);

--
-- Dumping data for table `wholesale_discount`
--
INSERT INTO `wholesale_discount` (`wholesale_discount_id`, `wholesale_discount_percent`) VALUES
(1, 1.2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
